﻿using System;
using System.Data.SqlClient;

namespace CarRoomCW.Data.Models
{
    public class Client : Abstract.Model
    {
        public Guid? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime SignedOn { get; set; }
        public DateTime BirthDate { get; set; }

        public override object[] Params => new object[] { Id, FirstName, LastName, SignedOn, BirthDate };

        public override void FromReader(SqlDataReader reader)
        {
            Id = reader.GetGuid(0);
            FirstName = reader.GetString(1);
            LastName = reader.GetString(2);
            SignedOn = reader.GetDateTime(3);
            BirthDate = reader.GetDateTime(4);
        }
    }
}
