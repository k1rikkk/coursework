﻿using System;
using System.Data.SqlClient;

namespace CarRoomCW.Data.Models
{
    public class Sell : Abstract.Model
    {
        public Guid? Id { get; set; }
        public Guid CarId { get; set; }
        public decimal Price { get; set; }
        public Guid ClientId { get; set; }
        public string Info { get; set; }
        public DateTime SoldOn { get; set; }

        public override object[] Params => new object[] { Id, CarId, Price, ClientId, Info, SoldOn };

        public override void FromReader(SqlDataReader reader)
        {
            Id = reader.GetGuid(0);
            CarId = reader.GetGuid(1);
            Price = reader.GetDecimal(2);
            ClientId = reader.GetGuid(3);
            Info = reader.GetString(4);
            SoldOn = reader.GetDateTime(5);
        }
    }
}
