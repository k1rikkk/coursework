﻿using System;
using System.Data.SqlClient;

namespace CarRoomCW.Data.Models
{
    public class Feature : Abstract.Model
    {
        public Guid? Id { get; set; }
        public string Title { get; set; }
        public string Info { get; set; }

        public override object[] Params => new object[] { Id, Title, Info };

        public override void FromReader(SqlDataReader reader)
        {
            Id = reader.GetGuid(0);
            Title = reader.GetString(1);
            Info = reader.GetString(2);
        }

        public override string ToString() => Title;
    }
}
