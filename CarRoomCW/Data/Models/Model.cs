﻿using System;
using System.Data.SqlClient;

namespace CarRoomCW.Data.Models
{
    public class Model : Abstract.Model
    {
        public Guid? Id { get; set; }
        public string Title { get; set; }
        public Guid BrandId { get; set; }
        public string ComfortClass { get; set; }

        public Brand Brand { get; set; }

        public override object[] Params => new object[] { Id, Title, BrandId, ComfortClass };

        public override void FromReader(SqlDataReader reader)
        {
            Id = reader.GetGuid(0);
            Title = reader.GetString(1);
            BrandId = reader.GetGuid(2);
            ComfortClass = reader.GetString(3);
        }

        public override string ToString() => /*Brand.Title + */Title;
    }
}
