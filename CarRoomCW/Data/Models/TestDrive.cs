﻿using System;
using System.Data.SqlClient;

namespace CarRoomCW.Data.Models
{
    public class TestDrive : Abstract.Model
    {
        public Guid? Id { get; set; }
        public Guid CarId { get; set; }
        public Guid ClientId { get; set; }
        public Guid? EmployeeId { get; set; }
        public decimal Price { get; set; }
        public DateTime BeginOn { get; set; }
        public DateTime EndOn { get; set; }
        public int Kilometrage { get; set; }

        public override object[] Params => new object[] { Id, CarId, ClientId, EmployeeId, Price, BeginOn, EndOn, Kilometrage };

        public override void FromReader(SqlDataReader reader)
        {
            Id = reader.GetGuid(0);
            CarId = reader.GetGuid(1);
            ClientId = reader.GetGuid(2);
            EmployeeId = reader.IsDBNull(3) ? null : (Guid?)reader.GetGuid(3);
            Price = reader.GetDecimal(4);
            BeginOn = reader.GetDateTime(5);
            EndOn = reader.GetDateTime(6);
            Kilometrage = reader.GetInt32(7);
        }
    }
}
