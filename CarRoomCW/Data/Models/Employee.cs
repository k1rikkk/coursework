﻿using System;
using System.Data.SqlClient;

namespace CarRoomCW.Data.Models
{
    public class Employee : Abstract.Model
    {
        public Guid? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid PostId { get; set; }
        public bool IsFired { get; set; }

        public Post Post { get; set; }

        public override object[] Params => new object[] { Id, FirstName, LastName, PostId, IsFired };

        public override void FromReader(SqlDataReader reader)
        {
            Id = reader.GetGuid(0);
            FirstName = reader.GetString(1);
            LastName = reader.GetString(2);
            PostId = reader.GetGuid(3);
            IsFired = reader.GetBoolean(4);
        }
    }
}
