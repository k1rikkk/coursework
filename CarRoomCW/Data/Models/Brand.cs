﻿using CarRoomCW.Data.Abstract;
using System;
using System.Data.SqlClient;

namespace CarRoomCW.Data.Models
{
    public class Brand : Abstract.Model
    {
        public Guid? Id { get; set; }
        public string Title { get; set; }
        public string Country { get; set; }

        public override object[] Params => new object[] { Id, Title, Country };

        public override void FromReader(SqlDataReader reader)
        {
            Id = reader.GetGuid(0);
            Title = reader.GetString(1);
            Country = reader.GetString(2);
        }

        public override string ToString() => Title;
    }
}
