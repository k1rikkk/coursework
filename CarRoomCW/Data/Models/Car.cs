﻿using System;
using System.Data.SqlClient;

namespace CarRoomCW.Data.Models
{
    public class Car : Abstract.Model
    {
        public Guid? Id { get; set; }
        public Guid ModelId { get; set; }
        public string LicenseCode { get; set; }
        public int Kilometrage { get; set; }
        public decimal Price { get; set; }
        public DateTime ManufacturedOn { get; set; }

        public Model Model { get; set; }

        public override object[] Params => new object[] { Id, ModelId, LicenseCode, Kilometrage, Price, ManufacturedOn };

        public override void FromReader(SqlDataReader reader)
        {
            Id = reader.GetGuid(0);
            ModelId = reader.GetGuid(1);
            LicenseCode = reader.IsDBNull(2) ? null : reader.GetString(2);
            Kilometrage = reader.GetInt32(3);
            Price = reader.GetDecimal(4);
            ManufacturedOn = reader.GetDateTime(5);
        }
    }
}
