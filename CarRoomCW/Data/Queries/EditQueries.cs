﻿namespace CarRoomCW.Data.Queries
{
    public class EditQueries
    {
        public string AddFeature { get; set; }
        public string AddFeatures { get; set; }
        public string RemoveFeatures { get; set; }
        public string InsertBrand { get; set; }
        public string InsertModel { get; set; }
        public string InsertCar { get; set; }
        public string InsertFeature { get; set; }
        public string CarRemove { get; set; }
        public string UpdateCar { get; set; }
        public string ClientRemove { get; set; }
        public string InsertClient { get; set; }
        public string UpdateClient { get; set; }
        public string SellRemove { get; set; }
        public string InsertSell { get; set; }
        public string UpdateSell { get; set; }
        public string TestDriveRemove { get; set; }
        public string InsertTestDrive { get; set; }
        public string UpdateTestDrive { get; set; }
        public string UpdateBrand { get; set; }
        public string BrandRemove { get; set; }
        public string ModelRemove { get; set; }
        public string UpdateModel { get; set; }
        public string FeatureRemove { get; set; }
        public string UpdateFeature { get; set; }
    }
}
