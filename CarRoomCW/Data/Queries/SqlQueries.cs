﻿using Newtonsoft.Json;
using System.IO;

namespace CarRoomCW.Data.Queries
{
    public sealed class SqlQueries
    {
        public static SqlQueries Instance
        {
            get
            {
                if (instance != null)
                    return instance;
                instance = new SqlQueries();
                return instance;
            }
        }
        
        public EditQueries Edit { get; }
        public SelectQueries Select { get; }

        private static SqlQueries instance;

        public SqlQueries()
        {
            Edit = JsonConvert.DeserializeObject<EditQueries>(File.ReadAllText("edit-queries.json"));
            Select = JsonConvert.DeserializeObject<SelectQueries>(File.ReadAllText("select-queries.json"));
        }
    }
}
