﻿namespace CarRoomCW.Data.Queries
{
    public class SelectQueries
    {
        public string ModelsQuery { get; set; }
        public string BrandsQuery { get; set; }
        public string CarFeaturesQuery { get; set; }
        public string FindFeaturesQuery { get; set; }
        public string BrandModelsQuery { get; set; }
        public string CarQuery { get; set; }
        public string ModelQuery { get; set; }
        public string BrandQuery { get; set; }
        public string CarsFormQuery { get; set; }
        public string CarsCount { get; set; }
        public string ClientsCount { get; set; }
        public string ClientsFormQuery { get; set; }
        public string ClientQuery { get; set; }
        public string SellQuery { get; set; }
        public string SellsFormQuery { get; set; }
        public string SellsCount { get; set; }
        public string TestDrivesFormQuery { get; set; }
        public string TestDriveQuery { get; set; }
        public string TestDrivesCount { get; set; }
        public string EmployeeQuery { get; set; }
        public string PostQuery { get; set; }
        public string FindEmployeesQuery { get; set; }
        public string CarsReportQuery { get; set; }
        public string GeneralReportQuery { get; set; }
        public string SellCarsReportQuery { get; set; }
        public string FeaturesQuery { get; set; }
    }
}
