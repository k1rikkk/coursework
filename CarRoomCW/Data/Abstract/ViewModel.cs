﻿using System;
using System.Windows.Forms;

namespace CarRoomCW.Data.Abstract
{
    public abstract class ViewModel
    {
        public Guid Id { get; set; }
        public Control Control { get; set; }
    }
}
