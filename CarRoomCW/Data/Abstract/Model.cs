﻿using System.Data.SqlClient;

namespace CarRoomCW.Data.Abstract
{
    public abstract class Model
    {
        public abstract object[] Params { get; }
        public abstract void FromReader(SqlDataReader reader);
    }
}
