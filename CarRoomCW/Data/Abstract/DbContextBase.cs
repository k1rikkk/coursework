﻿using CarRoomCW.Data.Models;
using CarRoomCW.Data.Queries;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarRoomCW.Data.Abstract
{
    public class DbContextBase : IDisposable
    {
        public DbContext DbContext { get; }
        public SqlQueries Queries => SqlQueries.Instance;

        public DbContextBase() => DbContext = new DbContext();

        public void Dispose() => DbContext.Dispose();

        protected virtual async Task<int> ValueCommand(string command, object value) => await DbContext.Command(command).UseParams(value).NonQueryAsync();

        protected virtual async Task<int> ModelCommand(string command, Model model) => await DbContext.Command(command).UseParams(model.Params).NonQueryAsync();

        protected virtual async Task<IEnumerable<TModel>> Set<TModel>(string command) where TModel : Model, new()
        {
            var result = new List<TModel>();

            using (var reader = await DbContext.Command(command).ReaderAsync())
                while (await reader.ReadAsync())
                {
                    var model = new TModel();
                    model.FromReader(reader);
                    result.Add(model);
                }

            return result;
        }

        protected virtual async Task<IEnumerable<TModel>> Set<TModel>(string command, object param) where TModel : Model, new()
        {
            var result = new List<TModel>();

            using (var reader = await DbContext.Command(command).UseParams(param).ReaderAsync())
                while (await reader.ReadAsync())
                {
                    var cur = new TModel();
                    cur.FromReader(reader);
                    result.Add(cur);
                }

            return result;
        }

        protected async Task<TModel> Single<TModel>(string command, Guid id) where TModel : Model, new()
        {
            using (var reader = await DbContext.Command(command).UseParams(id).ReaderAsync())
            {
                if (!reader.Read())
                    return null;

                var model = new TModel();
                model.FromReader(reader);
                return model;
            }
        }
    }
}
