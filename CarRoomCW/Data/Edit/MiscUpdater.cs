﻿using CarRoomCW.Data.Abstract;
using CarRoomCW.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace CarRoomCW.Data.Edit
{
    public class MiscUpdater : DbContextBase
    {
        public async Task UpdateFeatures(Guid carId, IEnumerable<Feature> features)
        {
            await RemoveFeatures(carId);
            
            var addings = new string[features.Count()];
            for (var i = 0; i < features.Count(); i++)
            {
                addings[i] = string.Format(Queries.Edit.AddFeature, "@0", "@" + (i + 1));
            }

            var sql = Queries.Edit.AddFeatures + string.Join(", ", addings);
            var vals = new[] { carId }.Concat(features.Select(f => f.Id.Value)).Select(f => (object)f).ToArray();

            await DbContext.Command(sql).UseValues(vals).NonQueryAsync();
        }

        public async Task RemoveFeatures(Guid carId) => await DbContext.Command(Queries.Edit.RemoveFeatures).UseParams(carId).NonQueryAsync();
    }
}
