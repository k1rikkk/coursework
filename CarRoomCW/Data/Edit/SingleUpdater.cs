﻿using CarRoomCW.Data.Abstract;
using CarRoomCW.Data.Models;
using System.Threading.Tasks;

namespace CarRoomCW.Data.Edit
{
    public class SingleUpdater : DbContextBase
    {
        public async Task<int> Car(Car car) => await ModelCommand(Queries.Edit.UpdateCar, car);
        public async Task<int> Client(Client client) => await ModelCommand(Queries.Edit.UpdateClient, client);
        public async Task<int> Sell(Sell sell) => await ModelCommand(Queries.Edit.UpdateSell, sell);
        public async Task<int> TestDrive(TestDrive testDrive) => await ModelCommand(Queries.Edit.UpdateTestDrive, testDrive);
        public async Task<int> Brand(Brand brand) => await ModelCommand(Queries.Edit.UpdateBrand, brand);
        public async Task<int> Model(Models.Model model) => await ModelCommand(Queries.Edit.UpdateModel, model);
        public async Task<int> Feature(Feature feature) => await ModelCommand(Queries.Edit.UpdateFeature, feature);
    }
}
