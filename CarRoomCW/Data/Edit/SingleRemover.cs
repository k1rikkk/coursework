﻿using CarRoomCW.Data.Abstract;
using CarRoomCW.Data.Models;
using System;
using System.Threading.Tasks;

namespace CarRoomCW.Data.Edit
{
    public class SingleRemover : DbContextBase
    {
        public async Task<int> ByType(Guid id, Type type)
        {
            if (type == typeof(Car))
                return await Car(id);
            else if (type == typeof(Client))
                return await Client(id);
            else if (type == typeof(Sell))
                return await Sell(id);
            else if (type == typeof(TestDrive))
                return await TestDrive(id);
            else if (type == typeof(Brand))
                return await Brand(id);
            else if (type == typeof(Models.Model))
                return await Model(id);
            else if (type == typeof(Feature))
                return await Feature(id);
            throw new NotImplementedException();
        }

        public async Task<int> Car(Guid carId) => await ValueCommand(Queries.Edit.CarRemove, carId);
        public async Task<int> Car(Car car) => await Car(car.Id.Value);
        public async Task<int> Client(Guid clientId) => await ValueCommand(Queries.Edit.ClientRemove, clientId);
        public async Task<int> Client(Client client) => await Client(client.Id.Value);
        public async Task<int> Sell(Guid sellId) => await ValueCommand(Queries.Edit.SellRemove, sellId);
        public async Task<int> Sell(Sell sell) => await Sell(sell.Id.Value);
        public async Task<int> TestDrive(Guid testDriveId) => await ValueCommand(Queries.Edit.TestDriveRemove, testDriveId);
        public async Task<int> TestDrive(TestDrive testDrive) => await TestDrive(testDrive.Id.Value);
        public async Task<int> Brand(Guid brandId) => await ValueCommand(Queries.Edit.BrandRemove, brandId);
        public async Task<int> Brand(Brand brand) => await Brand(brand.Id.Value);
        public async Task<int> Model(Guid modelId) => await ValueCommand(Queries.Edit.ModelRemove, modelId);
        public async Task<int> Model(Models.Model model) => await Model(model.Id.Value);
        public async Task<int> Feature(Guid featureId) => await ValueCommand(Queries.Edit.FeatureRemove, featureId);
        public async Task<int> Feature(Feature feature) => await Feature(feature.Id.Value);
    }
}
