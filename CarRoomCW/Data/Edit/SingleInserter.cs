﻿using CarRoomCW.Data.Abstract;
using CarRoomCW.Data.Models;
using System.Threading.Tasks;

namespace CarRoomCW.Data.Edit
{
    public class SingleInserter : DbContextBase
    {        
        public async Task<int> Insert(Brand brand) => await ModelCommand(Queries.Edit.InsertBrand, brand);
        public async Task<int> Insert(Models.Model model) => await ModelCommand(Queries.Edit.InsertModel, model);
        public async Task<int> Insert(Car car) => await ModelCommand(Queries.Edit.InsertCar, car);
        public async Task<int> Insert(Feature feature) => await ModelCommand(Queries.Edit.InsertFeature, feature);
        public async Task<int> Insert(Client client) => await ModelCommand(Queries.Edit.InsertClient, client);
        public async Task<int> Insert(Sell sell) => await ModelCommand(Queries.Edit.InsertSell, sell);
        public async Task<int> Insert(TestDrive testDrive) => await ModelCommand(Queries.Edit.InsertTestDrive, testDrive);
    }
}
