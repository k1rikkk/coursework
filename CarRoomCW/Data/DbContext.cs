﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace CarRoomCW.Data
{
    public class DbContext : IDisposable
    {
        public SqlConnection Connection { get; }

        public DbContext()
        {
            var connectionString = "Server=DESKTOP-Q7ANABN;Database=CarRoomDB;Trusted_Connection=True;";
            Connection = new SqlConnection(connectionString);
            Connection.Open();
        }

        public DbCommand Command(string command) => new DbCommand(command, Connection);

        public void Dispose() => Connection.Dispose();
    }
}
