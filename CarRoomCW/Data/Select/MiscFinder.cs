﻿using CarRoomCW.Data.Abstract;
using CarRoomCW.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarRoomCW.Data.Select
{
    public class MiscFinder : DbContextBase
    {
        public async Task<IEnumerable<Models.Model>> BrandModels(Guid brandId) => await Set<Models.Model>(Queries.Select.BrandModelsQuery, brandId);
        public async Task<IEnumerable<Feature>> CarFeatures(Guid carId) => await Set<Feature>(Queries.Select.CarFeaturesQuery, carId);
        public async Task<IEnumerable<Feature>> FindFeatures(string query) => await Set<Feature>(Queries.Select.FindFeaturesQuery, "%" + query + "%");
        public async Task<IEnumerable<Employee>> FindEmployees(string query)
        {
            var result = new List<Employee>();

            using (var reader = await DbContext.Command(Queries.Select.FindEmployeesQuery).UseParams("%" + query + "%").ReaderAsync())
                while (await reader.ReadAsync())
                {
                    var cur = new Employee();
                    cur.FromReader(reader);

                    cur.Post = new Post
                    {
                        Id = reader.GetGuid(5),
                        Title = reader.GetString(6),
                        Info = reader.GetString(7)
                    };

                    result.Add(cur);
                }

            return result;
        }
    }
}
