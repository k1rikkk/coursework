﻿using CarRoomCW.Data.Abstract;
using CarRoomCW.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarRoomCW.Data.Select
{
    public class AllFinder : DbContextBase
    {
        public async Task<IEnumerable<Brand>> Brands() => await Set<Brand>(Queries.Select.BrandsQuery);
        public async Task<IEnumerable<Models.Model>> Models() => await Set<Models.Model>(Queries.Select.ModelsQuery);
        public async Task<IEnumerable<Feature>> Features() => await Set<Feature>(Queries.Select.FeaturesQuery);
    }
}
