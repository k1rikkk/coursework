﻿using CarRoomCW.Data.Abstract;
using CarRoomCW.Data.Models;
using System;
using System.Threading.Tasks;

namespace CarRoomCW.Data.Select
{
    public class SingleFinder : DbContextBase
    {
        public async Task<Car> CarOrDefault(Guid id) => await Single<Car>(Queries.Select.CarQuery, id);

        public async Task<Models.Model> ModelOrDefault(Guid id) => await Single<Models.Model>(Queries.Select.ModelQuery, id);

        public async Task<Brand> BrandOrDefault(Guid id) => await Single<Brand>(Queries.Select.BrandQuery, id);

        public async Task<Client> ClientOrDefault(Guid id) => await Single<Client>(Queries.Select.ClientQuery, id);

        public async Task<Sell> SellOrDefault(Guid id) => await Single<Sell>(Queries.Select.SellQuery, id);

        public async Task<Employee> EmployeeOrDefault(Guid id) => await Single<Employee>(Queries.Select.EmployeeQuery, id);

        public async Task<Post> PostOrDefault(Guid id) => await Single<Post>(Queries.Select.PostQuery, id);

        public async Task<TestDrive> TestDriveOrDefault(Guid id) => await Single<TestDrive>(Queries.Select.TestDriveQuery, id);

        public async Task<Car> Car(Guid id) => await CarOrDefault(id) ?? throw new Exception("Car not found");

        public async Task<Models.Model> Model(Guid id) => await ModelOrDefault(id) ?? throw new Exception("Model not found");

        public async Task<Brand> Brand(Guid id) => await BrandOrDefault(id) ?? throw new Exception("Brand not found");

        public async Task<Client> Client(Guid id) => await ClientOrDefault(id) ?? throw new Exception("Client not found");

        public async Task<Sell> Sell(Guid id) => await SellOrDefault(id) ?? throw new Exception("Sell not found");

        public async Task<Employee> Employee(Guid id) => await EmployeeOrDefault(id) ?? throw new Exception("Employee not found");

        public async Task<Post> Post(Guid id) => await PostOrDefault(id) ?? throw new Exception("Post not found");

        public async Task<TestDrive> TestDrive(Guid id) => await TestDriveOrDefault(id) ?? throw new Exception("Test drive not found");
    }
}
