﻿using CarRoomCW.Data.Abstract;
using CarRoomCW.Data.ViewModels.Reports;
using System;
using System.Threading.Tasks;

namespace CarRoomCW.Data.Select
{
    public class ReportsBuilder : DbContextBase
    {
        public async Task<CarReport> Cars(DateTime begin, DateTime end)
        {
            var report = new CarReport()
            {
                Begin = begin,
                End = end
            };
            using (var reader = await DbContext.Command(Queries.Select.CarsReportQuery).UseParams(begin, end).ReaderAsync())
            {
                while (await reader.ReadAsync() && !reader.IsDBNull(0))
                {
                    var brand = new CarReport.Brand() { Title = reader.GetString(0) };
                    do
                    {
                        var model = new CarReport.Model() { Title = reader.GetString(1) };
                        do
                        {
                            model.Rows.Add(new CarReport.Row()
                            {
                                Client = reader.GetString(2),
                                SoldOn = reader.GetDateTime(3),
                                Price = reader.GetDecimal(4)
                            });
                        }
                        while (await reader.ReadAsync() && !reader.IsDBNull(2));
                        model.Price = reader.GetDecimal(4);
                        brand.Models.Add(model);
                    }
                    while (await reader.ReadAsync() && !reader.IsDBNull(1));
                    brand.Price = reader.GetDecimal(4);
                    report.Brands.Add(brand);
                }
                report.Price = reader.GetDecimal(4);
            }
            return report;
        }

        public async Task<GeneralReport> General(int year)
        {
            var report = new GeneralReport()
            {
                Year = year
            };
            using (var reader = await DbContext.Command(Queries.Select.GeneralReportQuery).UseParams(year).ReaderAsync())
            {
                while (await reader.ReadAsync() && !reader.IsDBNull(0))
                {
                    var month = new GeneralReport.Month() { MonthId = reader.GetInt32(0) };
                    if (!reader.IsDBNull(1))
                    {
                        do
                        {
                            month.Rows.Add(new GeneralReport.Row()
                            {
                                Brand = reader.GetString(1),
                                Model = reader.GetString(2),
                                Client = reader.GetString(3),
                                Price = reader.GetDecimal(4)
                            });
                        }
                        while (await reader.ReadAsync() && !reader.IsDBNull(1));
                    }
                    month.Price = reader.GetDecimal(4);
                    report.Months.Add(month);
                }
                report.Price = reader.GetDecimal(4);
            }
            return report;
        }

        public async Task<SellCarsReport> SellCars()
        {
            var report = new SellCarsReport();
            using (var reader = await DbContext.Command(Queries.Select.SellCarsReportQuery).ReaderAsync())
            {
                while (await reader.ReadAsync() && !reader.IsDBNull(0))
                {
                    var brand = new SellCarsReport.Brand() { Title = reader.GetString(0) };
                    do
                    {
                        var model = new SellCarsReport.Model() { Title = reader.GetString(1) };
                        do
                        {
                            model.Rows.Add(new SellCarsReport.Row()
                            {
                                Features = reader.GetString(3),
                                ManufacturedOn = reader.GetDateTime(2),
                                Price = reader.GetDecimal(4)
                            });
                        }
                        while (await reader.ReadAsync() && !reader.IsDBNull(2));
                        model.Price = reader.GetDecimal(4);
                        brand.Models.Add(model);
                    }
                    while (await reader.ReadAsync() && !reader.IsDBNull(1));
                    brand.Price = reader.GetDecimal(4);
                    report.Brands.Add(brand);
                }
                report.Price = reader.GetDecimal(4);
            }
            return report;
        }
    }
}
