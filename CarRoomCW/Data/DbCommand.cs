﻿using System;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace CarRoomCW.Data
{
    public class DbCommand
    {
        public SqlCommand SqlCommand { get; set; }

        public DbCommand(string command, SqlConnection connection)
        {
            SqlCommand = new SqlCommand(command, connection);
        }

        public DbCommand AddParam(string name, object value)
        {
            SqlCommand.Parameters.Add(new SqlParameter(name, value ?? DBNull.Value));
            return this;
        }

        public DbCommand UseValues(params object[] values)
        {
            for (var i = 0; i < values.Length; i++)
                SqlCommand.Parameters.AddWithValue("@" + i, values[i] ?? DBNull.Value);
            return this;
        }

        public DbCommand UseParams(params object[] values)
        {
            for (var i = 0; i < values.Length; i++)
                SqlCommand.Parameters.Add(new SqlParameter("@" + i, values[i] ?? DBNull.Value));
            return this;
        }

        public async Task<object> ScalarAsync() => await ScalarAsync(CancellationToken.None);
        public async Task<object> ScalarAsync(CancellationToken cancellationToken) => await SqlCommand.ExecuteScalarAsync(cancellationToken);

        public async Task<SqlDataReader> ReaderAsync() => await ReaderAsync(CancellationToken.None);
        public async Task<SqlDataReader> ReaderAsync(CancellationToken cancellationToken) => await SqlCommand.ExecuteReaderAsync(cancellationToken);

        public async Task<int> NonQueryAsync() => await NonQueryAsync(CancellationToken.None);
        public async Task<int> NonQueryAsync(CancellationToken cancellationToken) => await SqlCommand.ExecuteNonQueryAsync(cancellationToken);
    }
}
