﻿using CarRoomCW.Data.ViewModels.Reports;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace CarRoomCW.Data
{
    public static class ReportsWriter
    {
        public const string CarReportTemplate = "CarReportTemplate.docx";
        public const string GeneralReportTemplate = "GeneralReportTemplate.docx";
        public const string SellCarsReportTemplate = "SellCarsReportTemplate.docx";

        public static void WriteCarReport(string fileName, CarReport report)
        {
            File.Copy(Path.Combine(Application.StartupPath, CarReportTemplate), fileName, true);
            byte[] file = File.ReadAllBytes(fileName);
            using (var stream = new MemoryStream())
            {
                stream.Write(file, 0, file.Length);
                using (var doc = WordprocessingDocument.Open(stream, true))
                {
                    var main = doc.MainDocumentPart.Document;

                    var headerTable = main.Body.Elements<Table>().First();
                    headerTable.Elements<TableRow>().First().SetText(0, "Отчет о проданных автомобилях с " + report.Begin.ToString("dd.MM.yyyy") + " по " + report.End.ToString("dd.MM.yyyy"));
                    headerTable.Elements<TableRow>().ElementAt(1).SetText(0, "Общая стоимость всех проданных автомобилей : " + report.Price.ToString("n") + " Br");

                    var table = main.Body.Elements<Table>().ElementAt(1);

                    var summaryRow = (TableRow)table.Elements<TableRow>().Last().Clone();
                    var brandSummaryRow = (TableRow)table.Elements<TableRow>().ElementAt(2).Clone();
                    var modelSummaryRow = (TableRow)table.Elements<TableRow>().ElementAt(1).Clone();
                    var carRow = (TableRow)table.Elements<TableRow>().First().Clone();
                    TableRow row = null;

                    table.RemoveAllChildren();

                    foreach (var brand in report.Brands)
                    {
                        foreach (var model in brand.Models)
                        {
                            foreach (var car in model.Rows)
                            {
                                row = (TableRow)carRow.Clone();
                                row.SetText(0, brand.Title);
                                row.SetText(1, model.Title);
                                row.SetText(2, car.Client);
                                row.SetText(3, car.SoldOn.ToString("dd.MM.yyyy"));
                                row.SetText(4, car.Price.ToString("n") + " Br");
                                table.AppendChild(row);
                            }

                            row = (TableRow)modelSummaryRow.Clone();
                            row.SetText(2, "Общая стоимость модели " + model.Title);
                            row.SetText(3, model.Price.ToString("n") + " Br");
                            table.AppendChild(row);
                        }

                        row = (TableRow)brandSummaryRow.Clone();
                        row.SetText(1, "Общая стоимость марки " + brand.Title);
                        row.SetText(2, brand.Price.ToString("n") + " Br");
                        table.AppendChild(row);
                    }

                    summaryRow.SetText(0, "Общая стоимость всех проданных автомобилей");
                    summaryRow.SetText(1, report.Price.ToString("n") + " Br");
                    table.AppendChild(summaryRow);
                }
                File.WriteAllBytes(fileName, stream.ToArray());
            }
        }

        public static void WriteGeneralReport(string fileName, GeneralReport report)
        {
            File.Copy(Path.Combine(Application.StartupPath, GeneralReportTemplate), fileName, true);
            byte[] file = File.ReadAllBytes(fileName);
            using (var stream = new MemoryStream())
            {
                stream.Write(file, 0, file.Length);
                using (var doc = WordprocessingDocument.Open(stream, true))
                {
                    var main = doc.MainDocumentPart.Document;

                    var headerTable = main.Body.Elements<Table>().First();
                    headerTable.Elements<TableRow>().First().SetText(0, "Отчет о продажах автомобилей по месяцам " + report.Year + " года");
                    headerTable.Elements<TableRow>().ElementAt(1).SetText(0, "Общая стоимость всех проданных автомобилей : " + report.Price.ToString("n") + " Br");

                    var table = main.Body.Elements<Table>().ElementAt(1);

                    var summaryRow = (TableRow)table.Elements<TableRow>().Last().Clone();
                    var monthSummaryRow = (TableRow)table.Elements<TableRow>().ElementAt(1).Clone();
                    var carRow = (TableRow)table.Elements<TableRow>().First().Clone();
                    TableRow row = null;

                    table.RemoveAllChildren();

                    foreach (var month in report.Months)
                    {
                        if (month.Rows.Count == 0)
                            continue;

                        row = (TableRow)carRow.Clone();
                        row.SetText(0, month.Title);
                        row.SetText(1, month.Rows[0].Brand);
                        row.SetText(2, month.Rows[0].Model);
                        row.SetText(3, month.Rows[0].Client);
                        row.SetText(4, month.Rows[0].Price.ToString("n") + " Br");
                        table.Append(row);

                        foreach (var car in month.Rows.Skip(1))
                        {
                            row = (TableRow)carRow.Clone();
                            row.SetText(0, "");
                            row.SetText(1, car.Brand);
                            row.SetText(2, car.Model);
                            row.SetText(3, car.Client);
                            row.SetText(4, car.Price.ToString("n") + " Br");
                            table.Append(row);
                        }

                        row = (TableRow)monthSummaryRow.Clone();
                        row.SetText(1, "Общая стоимость проданных автомобилей за " + month.Title);
                        row.SetText(2, month.Price.ToString("n") + " Br");
                        table.AppendChild(row);
                    }

                    summaryRow.SetText(0, "Общая стоимость всех проданных автомобилей");
                    summaryRow.SetText(1, report.Price.ToString("n") + " Br");
                    table.AppendChild(summaryRow);
                }
                File.WriteAllBytes(fileName, stream.ToArray());
            }
        }

        public static void WriteSellCarsReport(string fileName, SellCarsReport report)
        {
            File.Copy(Path.Combine(Application.StartupPath, SellCarsReportTemplate), fileName, true);
            byte[] file = File.ReadAllBytes(fileName);
            using (var stream = new MemoryStream())
            {
                stream.Write(file, 0, file.Length);
                using (var doc = WordprocessingDocument.Open(stream, true))
                {
                    var main = doc.MainDocumentPart.Document;

                    var headerTable = main.Body.Elements<Table>().First();
                    headerTable.Elements<TableRow>().First().SetText(0, "Отчет об автомобилях в наличии на " + DateTime.Now.ToString("dd.MM.yyyy"));

                    var table = main.Body.Elements<Table>().ElementAt(1);

                    var summaryRow = (TableRow)table.Elements<TableRow>().Last().Clone();
                    var brandSummaryRow = (TableRow)table.Elements<TableRow>().ElementAt(2).Clone();
                    var modelSummaryRow = (TableRow)table.Elements<TableRow>().ElementAt(1).Clone();
                    var carRow = (TableRow)table.Elements<TableRow>().First().Clone();
                    TableRow row = null;

                    table.RemoveAllChildren();

                    foreach (var brand in report.Brands)
                    {
                        foreach (var model in brand.Models)
                        {
                            foreach (var car in model.Rows)
                            {
                                row = (TableRow)carRow.Clone();
                                row.SetText(0, brand.Title);
                                row.SetText(1, model.Title);
                                row.SetText(2, car.ManufacturedOn.ToString("MM.yyyy"));
                                row.SetText(3, car.Features);
                                row.SetText(4, car.Price.ToString("n") + " Br");
                                table.AppendChild(row);
                            }

                            row = (TableRow)modelSummaryRow.Clone();
                            row.SetText(2, "Общая стоимость модели " + model.Title);
                            row.SetText(3, model.Price.ToString("n") + " Br");
                            table.AppendChild(row);
                        }

                        row = (TableRow)brandSummaryRow.Clone();
                        row.SetText(1, "Общая стоимость марки " + brand.Title);
                        row.SetText(2, brand.Price.ToString("n") + " Br");
                        table.AppendChild(row);
                    }

                    summaryRow.SetText(0, "Общая стоимость всех автомобилей в наличии");
                    summaryRow.SetText(1, report.Price.ToString("n") + " Br");
                    table.AppendChild(summaryRow);
                }
                File.WriteAllBytes(fileName, stream.ToArray());
            }
        }

        public static void SetText(this TableRow row, int index, string text)
        {
            var a1 = row.Elements<TableCell>().ElementAt(index);
            var a2 = a1.Elements<Paragraph>().First();
            var a3 = a2.Elements<Run>().First();
            var a4 = a3.Elements<Text>().First();
            a4.Text = text;
        }
    }
}
