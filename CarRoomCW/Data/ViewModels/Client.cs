﻿using CarRoomCW.Data.Abstract;

namespace CarRoomCW.Data.ViewModels
{
    public class Client : ViewModel
    {
        public string Name { get; set; }
        public string BirthDate { get; set; }
    }
}
