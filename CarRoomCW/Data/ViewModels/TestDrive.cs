﻿using CarRoomCW.Data.Abstract;

namespace CarRoomCW.Data.ViewModels
{
    public class TestDrive : ViewModel
    {
        public string Client { get; set; }
        public string Car { get; set; }
        public string Employee { get; set; }
        public string Begin { get; set; }
        public string End { get; set; }
        public string Kilometrage { get; set; }
        public string Price { get; set; }
    }
}
