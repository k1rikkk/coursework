﻿using System.Collections.Generic;

namespace CarRoomCW.Data.ViewModels.Reports
{
    public class GeneralReport
    {
        public int Year { get; set; }
        public decimal Price { get; set; }
        public List<Month> Months { get; set; } = new List<Month>();

        public class Month
        {
            private static string[] Months { get; } = new[] { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };
         
            public int MonthId { get; set; }
            public string Title => Months[MonthId - 1];
            public List<Row> Rows { get; set; } = new List<Row>();
            public decimal Price { get; set; }
        }

        public class Row
        {
            public string Brand { get; set; }
            public string Model { get; set; }
            public string Client { get; set; }
            public decimal Price { get; set; }
        }
    }
}
