﻿using System;
using System.Collections.Generic;

namespace CarRoomCW.Data.ViewModels.Reports
{
    public class CarReport
    {
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }
        public List<Brand> Brands { get; set; } = new List<Brand>();
        public decimal Price { get; set; }

        public class Brand
        {
            public string Title { get; set; }
            public List<Model> Models { get; set; } = new List<Model>();
            public decimal Price { get; set; }
        }

        public class Model
        {
            public string Title { get; set; }
            public List<Row> Rows { get; set; } = new List<Row>();
            public decimal Price { get; set; }
        }

        public class Row
        {
            public string Client { get; set; }
            public DateTime SoldOn { get; set; }
            public decimal Price { get; set; }
        }
    }
}
