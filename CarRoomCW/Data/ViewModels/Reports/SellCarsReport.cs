﻿using System;
using System.Collections.Generic;

namespace CarRoomCW.Data.ViewModels.Reports
{
    public class SellCarsReport
    {
        public List<Brand> Brands { get; set; } = new List<Brand>();
        public decimal Price { get; set; }

        public class Brand
        {
            public string Title { get; set; }
            public List<Model> Models { get; set; } = new List<Model>();
            public decimal Price { get; set; }
        }

        public class Model
        {
            public string Title { get; set; }
            public List<Row> Rows { get; set; } = new List<Row>();
            public decimal Price { get; set; }
        }

        public class Row
        {
            public string Features { get; set; }
            public DateTime ManufacturedOn { get; set; }
            public decimal Price { get; set; }
        }
    }
}
