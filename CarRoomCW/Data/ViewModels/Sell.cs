﻿using CarRoomCW.Data.Abstract;

namespace CarRoomCW.Data.ViewModels
{
    public class Sell : ViewModel
    {
        public string Car { get; set; }
        public string Client { get; set; }
        public string Cost { get; set; }
        public string SoldOn { get; set; }
    }
}
