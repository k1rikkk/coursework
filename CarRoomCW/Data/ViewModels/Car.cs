﻿using CarRoomCW.Data.Abstract;
using System;
using System.Windows.Forms;

namespace CarRoomCW.Data.ViewModels
{
    public class Car : ViewModel
    {
        public string BrandModel { get; set; }
        public string License { get; set; }
        public int Kilometrage { get; set; }
        public DateTime Date { get; set; } 
        public string OwnerCost { get; set; }
    }
}
