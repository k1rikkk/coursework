﻿namespace CarRoomCW.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBoxData = new System.Windows.Forms.GroupBox();
            this.buttonAddTestDrive = new System.Windows.Forms.Button();
            this.buttonAddSell = new System.Windows.Forms.Button();
            this.buttonAddClient = new System.Windows.Forms.Button();
            this.buttonAddCar = new System.Windows.Forms.Button();
            this.buttonTestDrives = new System.Windows.Forms.Button();
            this.buttonParents = new System.Windows.Forms.Button();
            this.buttonSells = new System.Windows.Forms.Button();
            this.buttonClients = new System.Windows.Forms.Button();
            this.buttonCars = new System.Windows.Forms.Button();
            this.groupBoxSummary = new System.Windows.Forms.GroupBox();
            this.buttonReportSellCars = new System.Windows.Forms.Button();
            this.buttonReportCars = new System.Windows.Forms.Button();
            this.buttonSellsSummary = new System.Windows.Forms.Button();
            this.labelTime = new System.Windows.Forms.Label();
            this.timerTime = new System.Windows.Forms.Timer(this.components);
            this.groupBoxData.SuspendLayout();
            this.groupBoxSummary.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxData
            // 
            this.groupBoxData.Controls.Add(this.buttonAddTestDrive);
            this.groupBoxData.Controls.Add(this.buttonAddSell);
            this.groupBoxData.Controls.Add(this.buttonAddClient);
            this.groupBoxData.Controls.Add(this.buttonAddCar);
            this.groupBoxData.Controls.Add(this.buttonTestDrives);
            this.groupBoxData.Controls.Add(this.buttonParents);
            this.groupBoxData.Controls.Add(this.buttonSells);
            this.groupBoxData.Controls.Add(this.buttonClients);
            this.groupBoxData.Controls.Add(this.buttonCars);
            this.groupBoxData.Location = new System.Drawing.Point(18, 12);
            this.groupBoxData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBoxData.Name = "groupBoxData";
            this.groupBoxData.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBoxData.Size = new System.Drawing.Size(222, 253);
            this.groupBoxData.TabIndex = 1;
            this.groupBoxData.TabStop = false;
            this.groupBoxData.Text = "Данные";
            // 
            // buttonAddTestDrive
            // 
            this.buttonAddTestDrive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddTestDrive.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonAddTestDrive.Location = new System.Drawing.Point(179, 164);
            this.buttonAddTestDrive.Name = "buttonAddTestDrive";
            this.buttonAddTestDrive.Size = new System.Drawing.Size(35, 35);
            this.buttonAddTestDrive.TabIndex = 7;
            this.buttonAddTestDrive.Text = "+";
            this.buttonAddTestDrive.UseVisualStyleBackColor = true;
            this.buttonAddTestDrive.Click += new System.EventHandler(this.ButtonAddTestDrive_Click);
            // 
            // buttonAddSell
            // 
            this.buttonAddSell.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddSell.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonAddSell.Location = new System.Drawing.Point(179, 119);
            this.buttonAddSell.Name = "buttonAddSell";
            this.buttonAddSell.Size = new System.Drawing.Size(35, 35);
            this.buttonAddSell.TabIndex = 6;
            this.buttonAddSell.Text = "+";
            this.buttonAddSell.UseVisualStyleBackColor = true;
            this.buttonAddSell.Click += new System.EventHandler(this.ButtonAddSell_Click);
            // 
            // buttonAddClient
            // 
            this.buttonAddClient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddClient.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonAddClient.Location = new System.Drawing.Point(180, 74);
            this.buttonAddClient.Name = "buttonAddClient";
            this.buttonAddClient.Size = new System.Drawing.Size(35, 35);
            this.buttonAddClient.TabIndex = 5;
            this.buttonAddClient.Text = "+";
            this.buttonAddClient.UseVisualStyleBackColor = true;
            this.buttonAddClient.Click += new System.EventHandler(this.ButtonAddClient_Click);
            // 
            // buttonAddCar
            // 
            this.buttonAddCar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddCar.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonAddCar.Location = new System.Drawing.Point(180, 29);
            this.buttonAddCar.Name = "buttonAddCar";
            this.buttonAddCar.Size = new System.Drawing.Size(35, 35);
            this.buttonAddCar.TabIndex = 0;
            this.buttonAddCar.Text = "+";
            this.buttonAddCar.UseVisualStyleBackColor = true;
            this.buttonAddCar.Click += new System.EventHandler(this.ButtonAddCar_Click);
            // 
            // buttonTestDrives
            // 
            this.buttonTestDrives.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTestDrives.Location = new System.Drawing.Point(9, 164);
            this.buttonTestDrives.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonTestDrives.Name = "buttonTestDrives";
            this.buttonTestDrives.Size = new System.Drawing.Size(164, 35);
            this.buttonTestDrives.TabIndex = 4;
            this.buttonTestDrives.Text = "Тест драйвы";
            this.buttonTestDrives.UseVisualStyleBackColor = true;
            this.buttonTestDrives.Click += new System.EventHandler(this.ButtonTestDrives_Click);
            // 
            // buttonParents
            // 
            this.buttonParents.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonParents.Location = new System.Drawing.Point(9, 209);
            this.buttonParents.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonParents.Name = "buttonParents";
            this.buttonParents.Size = new System.Drawing.Size(205, 35);
            this.buttonParents.TabIndex = 3;
            this.buttonParents.Text = "Справочники";
            this.buttonParents.UseVisualStyleBackColor = true;
            this.buttonParents.Click += new System.EventHandler(this.ButtonParents_Click);
            // 
            // buttonSells
            // 
            this.buttonSells.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSells.Location = new System.Drawing.Point(8, 119);
            this.buttonSells.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonSells.Name = "buttonSells";
            this.buttonSells.Size = new System.Drawing.Size(164, 35);
            this.buttonSells.TabIndex = 2;
            this.buttonSells.Text = "Продажи";
            this.buttonSells.UseVisualStyleBackColor = true;
            this.buttonSells.Click += new System.EventHandler(this.ButtonSells_Click);
            // 
            // buttonClients
            // 
            this.buttonClients.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClients.Location = new System.Drawing.Point(9, 74);
            this.buttonClients.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonClients.Name = "buttonClients";
            this.buttonClients.Size = new System.Drawing.Size(164, 35);
            this.buttonClients.TabIndex = 1;
            this.buttonClients.Text = "Клиенты";
            this.buttonClients.UseVisualStyleBackColor = true;
            this.buttonClients.Click += new System.EventHandler(this.ButtonClients_Click);
            // 
            // buttonCars
            // 
            this.buttonCars.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCars.Location = new System.Drawing.Point(9, 29);
            this.buttonCars.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonCars.Name = "buttonCars";
            this.buttonCars.Size = new System.Drawing.Size(164, 35);
            this.buttonCars.TabIndex = 0;
            this.buttonCars.Text = "Автомобили";
            this.buttonCars.UseVisualStyleBackColor = true;
            this.buttonCars.Click += new System.EventHandler(this.ButtonCars_Click);
            // 
            // groupBoxSummary
            // 
            this.groupBoxSummary.Controls.Add(this.buttonReportSellCars);
            this.groupBoxSummary.Controls.Add(this.buttonReportCars);
            this.groupBoxSummary.Controls.Add(this.buttonSellsSummary);
            this.groupBoxSummary.Location = new System.Drawing.Point(247, 104);
            this.groupBoxSummary.Name = "groupBoxSummary";
            this.groupBoxSummary.Size = new System.Drawing.Size(264, 161);
            this.groupBoxSummary.TabIndex = 2;
            this.groupBoxSummary.TabStop = false;
            this.groupBoxSummary.Text = "Отчеты";
            // 
            // buttonReportSellCars
            // 
            this.buttonReportSellCars.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReportSellCars.Location = new System.Drawing.Point(7, 119);
            this.buttonReportSellCars.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonReportSellCars.Name = "buttonReportSellCars";
            this.buttonReportSellCars.Size = new System.Drawing.Size(250, 35);
            this.buttonReportSellCars.TabIndex = 11;
            this.buttonReportSellCars.Text = "Автомобили в продаже";
            this.buttonReportSellCars.UseVisualStyleBackColor = true;
            this.buttonReportSellCars.Click += new System.EventHandler(this.ButtonReportSellCars_Click);
            // 
            // buttonReportCars
            // 
            this.buttonReportCars.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReportCars.Location = new System.Drawing.Point(7, 74);
            this.buttonReportCars.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonReportCars.Name = "buttonReportCars";
            this.buttonReportCars.Size = new System.Drawing.Size(250, 35);
            this.buttonReportCars.TabIndex = 10;
            this.buttonReportCars.Text = "Продажи автомобилей";
            this.buttonReportCars.UseVisualStyleBackColor = true;
            this.buttonReportCars.Click += new System.EventHandler(this.ButtonReportCars_Click);
            // 
            // buttonSellsSummary
            // 
            this.buttonSellsSummary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSellsSummary.Location = new System.Drawing.Point(7, 29);
            this.buttonSellsSummary.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonSellsSummary.Name = "buttonSellsSummary";
            this.buttonSellsSummary.Size = new System.Drawing.Size(250, 35);
            this.buttonSellsSummary.TabIndex = 9;
            this.buttonSellsSummary.Text = "Общий";
            this.buttonSellsSummary.UseVisualStyleBackColor = true;
            this.buttonSellsSummary.Click += new System.EventHandler(this.ButtonSellsSummary_Click);
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Location = new System.Drawing.Point(311, 56);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(155, 20);
            this.labelTime.TabIndex = 11;
            this.labelTime.Text = "01.01.2019 02:00:00";
            // 
            // timerTime
            // 
            this.timerTime.Enabled = true;
            this.timerTime.Interval = 1000;
            this.timerTime.Tick += new System.EventHandler(this.TimerTime_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 283);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.groupBoxSummary);
            this.Controls.Add(this.groupBoxData);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.Text = "Автосалон";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.groupBoxData.ResumeLayout(false);
            this.groupBoxSummary.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxData;
        private System.Windows.Forms.Button buttonParents;
        private System.Windows.Forms.Button buttonSells;
        private System.Windows.Forms.Button buttonClients;
        private System.Windows.Forms.Button buttonCars;
        private System.Windows.Forms.Button buttonTestDrives;
        private System.Windows.Forms.Button buttonAddClient;
        private System.Windows.Forms.Button buttonAddCar;
        private System.Windows.Forms.GroupBox groupBoxSummary;
        private System.Windows.Forms.Button buttonAddTestDrive;
        private System.Windows.Forms.Button buttonAddSell;
        private System.Windows.Forms.Button buttonReportSellCars;
        private System.Windows.Forms.Button buttonReportCars;
        private System.Windows.Forms.Button buttonSellsSummary;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Timer timerTime;
    }
}

