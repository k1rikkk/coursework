﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using CarRoomCW.Data.Queries;
using CarRoomCW.Data.ViewModels;
using CarRoomCW.Forms.Abstract;

namespace CarRoomCW.Forms.TestDrives
{
    public class TestDrivesForm : ModelsForm<TestDrive, EditTestDriveForm>
    {
        public override string CountQuery => SqlQueries.Instance.Select.TestDrivesCount;
        public override string DataQuery => SqlQueries.Instance.Select.TestDrivesFormQuery;
        public override Size GridSize => new Size(3, 6);
        public override Size CellSize => new Size(277, 123);
        public override Size FormSize => new Size(1100, 600);

        public override string FormText => "Тест-драйвы";
        public override string SelectModelTitle => "Тест-драйв не выбран";
        public override string SelectModelText => "Выберите тест-драйв";
        public override string RemoveConfirmTitle => "Удаление тест-драйва";
        public override string RemoveConfirmText => "Вы уверены, что хотите удалить тест-драйв";

        public override Type ModelType => typeof(TestDrive);

        public override Control[] BuildInner(TestDrive model) => new Control[]
        {
            new Label()
            {
                Text = model.Client,
                Location = new Point(3, 4),
                Font = new Font("Microsoft Sans Serif", 14),
                Size = new Size(269, 22)
            },
            new Label()
            {
                Text = model.Car,
                Location = new Point(3, 27),
                Font = new Font("Microsoft Sans Serif", 14),
                Size = new Size(269, 22)
            },
            new Label()
            {
                Text = model.Employee,
                Location = new Point(3, 52),
                Font = new Font("Microsoft Sans Serif", 12),
                Size = new Size(269, 22)
            },
            new Label()
            {
                Text = model.Begin,
                Location = new Point(3, 78),
                Font = new Font("Microsoft Sans Serif", 10),
                Size = new Size(130, 22)
            },
            new Label()
            {
                Text = model.End,
                Location = new Point(141, 78),
                TextAlign = ContentAlignment.TopRight,
                Font = new Font("Microsoft Sans Serif", 10),
                Size = new Size(130, 22)
            },
            new Label()
            {
                Text = model.Price,
                Location = new Point(3, 98),
                Font = new Font("Microsoft Sans Serif", 10),
                Size = new Size(110, 20)
            },
            new Label()
            {
                Text = model.Kilometrage,
                Location = new Point(141, 98),
                TextAlign = ContentAlignment.TopRight,
                Font = new Font("Microsoft Sans Serif", 10),
                Size = new Size(130, 20)
            }
        };

        protected override TestDrive CreateModel(SqlDataReader reader) => new TestDrive()
        {
            Id = reader.GetGuid(0),
            Client = reader.GetString(1),
            Car = reader.GetString(2),
            Employee = reader.IsDBNull(3) ? "Без сотрудника" : reader.GetString(3),
            Begin = reader.GetDateTime(4).ToString("dd.MM.yyyy hh:mm"),
            End = reader.GetDateTime(5).ToString("dd.MM.yyyy hh:mm"),
            Kilometrage = reader.GetInt32(6).ToString("n0") + " км",
            Price = reader.GetDecimal(7).ToString("n") + " Br"
        };
    }
}
