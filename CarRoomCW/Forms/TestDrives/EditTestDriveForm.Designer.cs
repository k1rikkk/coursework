﻿namespace CarRoomCW.Forms.TestDrives
{
    partial class EditTestDriveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxClient = new System.Windows.Forms.TextBox();
            this.labelClient = new System.Windows.Forms.Label();
            this.textBoxCar = new System.Windows.Forms.TextBox();
            this.labelCar = new System.Windows.Forms.Label();
            this.textBoxEmployee = new System.Windows.Forms.TextBox();
            this.labelEmployee = new System.Windows.Forms.Label();
            this.labelCost = new System.Windows.Forms.Label();
            this.textBoxCost = new System.Windows.Forms.TextBox();
            this.textBoxBegin = new System.Windows.Forms.TextBox();
            this.labelBegin = new System.Windows.Forms.Label();
            this.textBoxEnd = new System.Windows.Forms.TextBox();
            this.labelEnd = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.textBoxKilometrage = new System.Windows.Forms.TextBox();
            this.labelKilometrage = new System.Windows.Forms.Label();
            this.labelCur = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxClient
            // 
            this.textBoxClient.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxClient.Cursor = System.Windows.Forms.Cursors.Hand;
            this.textBoxClient.Location = new System.Drawing.Point(173, 12);
            this.textBoxClient.Name = "textBoxClient";
            this.textBoxClient.ReadOnly = true;
            this.textBoxClient.Size = new System.Drawing.Size(512, 26);
            this.textBoxClient.TabIndex = 7;
            this.textBoxClient.Click += new System.EventHandler(this.TextBoxClient_Click);
            // 
            // labelClient
            // 
            this.labelClient.AutoSize = true;
            this.labelClient.Location = new System.Drawing.Point(12, 15);
            this.labelClient.Name = "labelClient";
            this.labelClient.Size = new System.Drawing.Size(65, 20);
            this.labelClient.TabIndex = 6;
            this.labelClient.Text = "Клиент";
            // 
            // textBoxCar
            // 
            this.textBoxCar.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxCar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.textBoxCar.Location = new System.Drawing.Point(173, 76);
            this.textBoxCar.Name = "textBoxCar";
            this.textBoxCar.ReadOnly = true;
            this.textBoxCar.Size = new System.Drawing.Size(512, 26);
            this.textBoxCar.TabIndex = 10;
            this.textBoxCar.Click += new System.EventHandler(this.TextBoxCar_Click);
            // 
            // labelCar
            // 
            this.labelCar.AutoSize = true;
            this.labelCar.Location = new System.Drawing.Point(13, 79);
            this.labelCar.Name = "labelCar";
            this.labelCar.Size = new System.Drawing.Size(104, 20);
            this.labelCar.TabIndex = 4;
            this.labelCar.Text = "Автомобиль";
            // 
            // textBoxEmployee
            // 
            this.textBoxEmployee.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxEmployee.Cursor = System.Windows.Forms.Cursors.Hand;
            this.textBoxEmployee.Location = new System.Drawing.Point(173, 44);
            this.textBoxEmployee.Name = "textBoxEmployee";
            this.textBoxEmployee.ReadOnly = true;
            this.textBoxEmployee.Size = new System.Drawing.Size(512, 26);
            this.textBoxEmployee.TabIndex = 9;
            this.textBoxEmployee.Click += new System.EventHandler(this.TextBoxEmployee_Click);
            // 
            // labelEmployee
            // 
            this.labelEmployee.AutoSize = true;
            this.labelEmployee.Location = new System.Drawing.Point(12, 47);
            this.labelEmployee.Name = "labelEmployee";
            this.labelEmployee.Size = new System.Drawing.Size(91, 20);
            this.labelEmployee.TabIndex = 8;
            this.labelEmployee.Text = "Сотрудник";
            // 
            // labelCost
            // 
            this.labelCost.AutoSize = true;
            this.labelCost.Location = new System.Drawing.Point(344, 111);
            this.labelCost.Name = "labelCost";
            this.labelCost.Size = new System.Drawing.Size(93, 20);
            this.labelCost.TabIndex = 10;
            this.labelCost.Text = "Стоимость";
            // 
            // textBoxCost
            // 
            this.textBoxCost.Location = new System.Drawing.Point(533, 108);
            this.textBoxCost.Name = "textBoxCost";
            this.textBoxCost.Size = new System.Drawing.Size(118, 26);
            this.textBoxCost.TabIndex = 12;
            // 
            // textBoxBegin
            // 
            this.textBoxBegin.Location = new System.Drawing.Point(173, 139);
            this.textBoxBegin.Name = "textBoxBegin";
            this.textBoxBegin.Size = new System.Drawing.Size(165, 26);
            this.textBoxBegin.TabIndex = 15;
            // 
            // labelBegin
            // 
            this.labelBegin.AutoSize = true;
            this.labelBegin.Location = new System.Drawing.Point(13, 142);
            this.labelBegin.Name = "labelBegin";
            this.labelBegin.Size = new System.Drawing.Size(158, 20);
            this.labelBegin.TabIndex = 14;
            this.labelBegin.Text = "Дата/время начала";
            // 
            // textBoxEnd
            // 
            this.textBoxEnd.Location = new System.Drawing.Point(533, 139);
            this.textBoxEnd.Name = "textBoxEnd";
            this.textBoxEnd.Size = new System.Drawing.Size(152, 26);
            this.textBoxEnd.TabIndex = 17;
            // 
            // labelEnd
            // 
            this.labelEnd.AutoSize = true;
            this.labelEnd.Location = new System.Drawing.Point(344, 142);
            this.labelEnd.Name = "labelEnd";
            this.labelEnd.Size = new System.Drawing.Size(183, 20);
            this.labelEnd.TabIndex = 16;
            this.labelEnd.Text = "Дата/время окончания";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(476, 171);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(209, 34);
            this.buttonCancel.TabIndex = 21;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(17, 171);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(252, 34);
            this.buttonSave.TabIndex = 19;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(275, 171);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(195, 34);
            this.buttonRemove.TabIndex = 20;
            this.buttonRemove.Text = "Удалить";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.ButtonRemove_Click);
            // 
            // textBoxKilometrage
            // 
            this.textBoxKilometrage.Location = new System.Drawing.Point(173, 108);
            this.textBoxKilometrage.Name = "textBoxKilometrage";
            this.textBoxKilometrage.Size = new System.Drawing.Size(165, 26);
            this.textBoxKilometrage.TabIndex = 11;
            // 
            // labelKilometrage
            // 
            this.labelKilometrage.AutoSize = true;
            this.labelKilometrage.Location = new System.Drawing.Point(12, 111);
            this.labelKilometrage.Name = "labelKilometrage";
            this.labelKilometrage.Size = new System.Drawing.Size(64, 20);
            this.labelKilometrage.TabIndex = 21;
            this.labelKilometrage.Text = "Пробег";
            // 
            // labelCur
            // 
            this.labelCur.Location = new System.Drawing.Point(657, 107);
            this.labelCur.Name = "labelCur";
            this.labelCur.Size = new System.Drawing.Size(28, 28);
            this.labelCur.TabIndex = 23;
            this.labelCur.Text = "Br";
            this.labelCur.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EditTestDriveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 220);
            this.Controls.Add(this.labelCur);
            this.Controls.Add(this.textBoxKilometrage);
            this.Controls.Add(this.labelKilometrage);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.textBoxEnd);
            this.Controls.Add(this.labelEnd);
            this.Controls.Add(this.textBoxBegin);
            this.Controls.Add(this.labelBegin);
            this.Controls.Add(this.textBoxCost);
            this.Controls.Add(this.labelCost);
            this.Controls.Add(this.textBoxEmployee);
            this.Controls.Add(this.labelEmployee);
            this.Controls.Add(this.textBoxClient);
            this.Controls.Add(this.labelClient);
            this.Controls.Add(this.textBoxCar);
            this.Controls.Add(this.labelCar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "EditTestDriveForm";
            this.Text = "Редактирование тест-драйва";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxClient;
        private System.Windows.Forms.Label labelClient;
        private System.Windows.Forms.TextBox textBoxCar;
        private System.Windows.Forms.Label labelCar;
        private System.Windows.Forms.TextBox textBoxEmployee;
        private System.Windows.Forms.Label labelEmployee;
        private System.Windows.Forms.Label labelCost;
        private System.Windows.Forms.TextBox textBoxCost;
        private System.Windows.Forms.TextBox textBoxBegin;
        private System.Windows.Forms.Label labelBegin;
        private System.Windows.Forms.TextBox textBoxEnd;
        private System.Windows.Forms.Label labelEnd;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.TextBox textBoxKilometrage;
        private System.Windows.Forms.Label labelKilometrage;
        private System.Windows.Forms.Label labelCur;
    }
}