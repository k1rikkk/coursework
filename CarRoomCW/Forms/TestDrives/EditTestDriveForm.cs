﻿using CarRoomCW.Data.Edit;
using CarRoomCW.Data.Models;
using CarRoomCW.Data.Select;
using CarRoomCW.Forms.Abstract;
using CarRoomCW.Forms.Cars;
using CarRoomCW.Forms.Clients;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRoomCW.Forms.TestDrives
{
    public partial class EditTestDriveForm : EditForm, ISelectionForm
    {
        private Car car;
        private Client client;
        private Employee employee;

        public EditTestDriveForm()
        {
            InitializeComponent();
        }

        public EditTestDriveForm(IDataForm dataForm) : this() => DataForm = dataForm;

        public async override Task SetAdding() => await Task.CompletedTask;

        public async override Task SetEdit(Guid id)
        {
            ModelId = id;

            TestDrive testDrive = null;
            using (var finder = new SingleFinder())
            {
                testDrive = await finder.TestDrive(id);
                car = await finder.Car(testDrive.CarId);
                car.Model = await finder.Model(car.ModelId);
                car.Model.Brand = await finder.Brand(car.Model.BrandId);
                client = await finder.Client(testDrive.ClientId);
                if (testDrive.EmployeeId.HasValue)
                {
                    employee = await finder.Employee(testDrive.EmployeeId.Value);
                    employee.Post = await finder.Post(employee.PostId);
                }
            }

            textBoxCar.Text = car.Model.Brand.Title + " " + car.Model.Title + " (" + car.LicenseCode + ")";
            textBoxClient.Text = client.LastName + " " + client.FirstName + " " + client.BirthDate.ToString("dd.MM.yyyy");
            textBoxEmployee.Text = employee == null ? "Без сотрудника" : "[" + employee.Post.Title + "] " + employee.LastName + " " + employee.FirstName;
            textBoxCost.Text = testDrive.Price.ToString("n");
            textBoxKilometrage.Text = testDrive.Kilometrage.ToString("N0");
            textBoxBegin.Text = testDrive.BeginOn.ToString("hh:mm dd.MM.yyyy");
            textBoxEnd.Text = testDrive.EndOn.ToString("hh:mm dd.MM.yyyy");
        }

        public async Task Select(Guid? id, Type type)
        {
            using (var finder = new SingleFinder())
            {
                if (type == typeof(Car))
                {
                    car = await finder.Car(id.Value);
                    car.Model = await finder.Model(car.ModelId);
                    car.Model.Brand = await finder.Brand(car.Model.BrandId);
                    textBoxCar.Text = car.Model.Brand.Title + " " + car.Model.Title + " (" + car.LicenseCode + ")";
                }
                else if (type == typeof(Client))
                {
                    client = await finder.Client(id.Value);
                    textBoxClient.Text = client.LastName + " " + client.FirstName + " " + client.BirthDate.ToString("dd.MM.yyyy");
                }
                else if (type == typeof(Employee))
                {
                    if (id == null)
                    {
                        employee = null;
                        textBoxEmployee.Text = "Без сотрудника";
                    }
                    else
                    {
                        employee = await finder.Employee(id.Value);
                        employee.Post = await finder.Post(employee.PostId);
                        textBoxEmployee.Text = "[" + employee.Post.Title + "] " + employee.LastName + " " + employee.FirstName;
                    }
                }
            }
        }

        private void TextBoxCar_Click(object sender, EventArgs e)
        {
            var form = new CarsForm();
            form.SelectionForm = this;
            form.Show();
        }

        private void TextBoxClient_Click(object sender, EventArgs e)
        {
            var form = new ClientsForm();
            form.SelectionForm = this;
            form.Show();
        }

        private void TextBoxEmployee_Click(object sender, EventArgs e) => new SelectEmployeeForm(this).Show();

        public async override Task Save()
        {
            if (car == null)
            {
                MessageBox.Show("Выберите автомобиль", "Неверный формат", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxCar.Focus();
                return;
            }
            if (client == null)
            {
                MessageBox.Show("Выберите клиента", "Неверный формат", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxClient.Focus();
                return;
            }
            if (!decimal.TryParse(textBoxCost.Text.Replace(" ", "").Replace(((char)160).ToString(), ""), out var price))
            {
                MessageBox.Show("Цена должна быть вещественным числом с запятой", "Неверный формат", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxCost.Focus();
                return;
            }
            if (!DateTime.TryParse(textBoxBegin.Text, out var begin))
            {
                MessageBox.Show("Введите дату начала в формате: <две цифры часов>:<две цифры минут> <две цифры дня>.<две цифры месяца>.<четыре цифры года>", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxBegin.Focus();
                return;
            }
            if (!DateTime.TryParse(textBoxBegin.Text, out var end))
            {
                MessageBox.Show("Введите дату окончания в формате: <две цифры часов>:<две цифры минут> <две цифры дня>.<две цифры месяца>.<четыре цифры года>", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxBegin.Focus();
                return;
            }
            if (!int.TryParse(textBoxKilometrage.Text.Replace(" ", "").Replace(((char)160).ToString(), ""), out var kilometrage))
            {
                MessageBox.Show("Пробег должен быть целым числом", "Неверный формат", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxKilometrage.Focus();
                return;
            }
            if (!ModelId.HasValue)
            {
                using (var inserter = new SingleInserter())
                    await inserter.Insert(new TestDrive()
                    {
                        Id = Guid.NewGuid(),
                        BeginOn = begin,
                        EndOn = end,
                        CarId = car.Id.Value,
                        ClientId = client.Id.Value,
                        EmployeeId = employee?.Id.Value,
                        Kilometrage = kilometrage,
                        Price = price
                    });
            }
            else
            {
                using (var updater = new SingleUpdater())
                    await updater.TestDrive(new TestDrive()
                    {
                        Id = ModelId.Value,
                        BeginOn = begin,
                        EndOn = end,
                        CarId = car.Id.Value,
                        ClientId = client.Id.Value,
                        EmployeeId = employee?.Id.Value,
                        Kilometrage = kilometrage,
                        Price = price
                    });
            }
            Close();
            if (DataForm != null)
            {
                await DataForm.ResetPages();
                await DataForm.ResetData();
            }
        }

        private async void ButtonSave_Click(object sender, EventArgs e) => await Save();

        private async void ButtonRemove_Click(object sender, EventArgs e) => await Remove();

        private void ButtonCancel_Click(object sender, EventArgs e) => Close();

        public async override Task RemoveSingle()
        {
            using (var remover = new SingleRemover())
                await remover.TestDrive(ModelId.Value);
        }
    }
}
