﻿using CarRoomCW.Data.Models;
using CarRoomCW.Data.Select;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRoomCW.Forms.TestDrives
{
    public partial class SelectEmployeeForm : Form
    {
        private IEnumerable<Employee> employees;
        private readonly EditTestDriveForm testDriveForm;

        public SelectEmployeeForm(EditTestDriveForm testDriveForm)
        {
            this.testDriveForm = testDriveForm;
            InitializeComponent();
        }

        private async Task ResetList()
        {
            using (var finder = new MiscFinder())
                employees = await finder.FindEmployees(textBoxQuery.Text);
            listBoxEmployees.Items.Clear();
            listBoxEmployees.Items.Add("Без сотрудника..");
            listBoxEmployees.Items.AddRange(employees.Select(e => "[" + e.Post.Title + "] " + e.LastName + " " + e.FirstName).ToArray());
            listBoxEmployees.SelectedIndex = listBoxEmployees.Items.Count > 1 ? 1 : 0;
        }

        private async void ButtonFind_Click(object sender, EventArgs e) => await ResetList();

        private async void TextBoxQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                await ResetList();
        }

        private async Task Save()
        {
            if (listBoxEmployees.SelectedIndex < 0)
            {
                MessageBox.Show("Выберите сотрудника", "сотрудник не выбрана", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (listBoxEmployees.SelectedIndex == 0)
                await testDriveForm.Select(null, typeof(Employee));
            else
                await testDriveForm.Select(employees.ElementAt(listBoxEmployees.SelectedIndex - 1).Id.Value, typeof(Employee));

            Close();
        }

        private async void SelectFeatureForm_Load(object sender, EventArgs e) => await ResetList();

        private async void ButtonOk_Click(object sender, EventArgs e) => await Save();

        private void ListBoxFeatures_DoubleClick(object sender, EventArgs e) => ButtonOk_Click(sender, e);

        private void ButtonCancel_Click(object sender, EventArgs e) => Close();

        private async void ButtonClear_Click(object sender, EventArgs e)
        {
            textBoxQuery.Clear();
            await ResetList();
        }
    }
}
