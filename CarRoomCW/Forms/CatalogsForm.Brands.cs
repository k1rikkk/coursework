﻿using CarRoomCW.Data.Edit;
using CarRoomCW.Data.Models;
using System;
using System.Threading.Tasks;

namespace CarRoomCW.Forms
{
    public partial class CatalogsForm
    {
        private Brand activeBrand;
        private Brand SelectedBrand => (Brand)listBoxBrands.SelectedItem;

        private async void ListBoxBrands_SelectedIndexChanged(object sender, EventArgs e)
        {
            await ResetModels(((Brand)listBoxBrands.SelectedItem).Id.Value);
        }

        public async Task ResetBrands()
        {
            listBoxBrands.Items.Clear();
            await ResetCatalog(finder => finder.Brands(), listBoxBrands);
            listBoxBrands.SelectedIndex = 0;
        }

        private void ButtonEditBrand_Click(object sender, EventArgs e)
        {
            activeBrand = (Brand)listBoxBrands.SelectedItem;
            textBoxBrand.Text = activeBrand.Title;
        }

        private async void ButtonRemoveBrand_Click(object sender, EventArgs e) => await RemoveModel(typeof(Brand), ((Brand)listBoxBrands.SelectedItem).Id.Value, "Вы уверены, что хотите удалить марку?", "Удаление марки", () => ResetBrands());

        private async void ButtonSaveBrand_Click(object sender, EventArgs e)
        {
            if (activeBrand != null)
            {
                activeBrand.Title = textBoxBrand.Text;
                using (var updater = new SingleUpdater())
                    await updater.Brand(activeBrand);
                activeBrand = null;
                textBoxBrand.Text = "";
            }
            else
            {
                var brand = new Brand
                {
                    Id = Guid.NewGuid(),
                    Title = textBoxBrand.Text,
                    Country = "Unset"
                };
                using (var inserter = new SingleInserter())
                    await inserter.Insert(brand);
                textBoxBrand.Text = "";
            }
            await ResetBrands();
        }
    }
}
