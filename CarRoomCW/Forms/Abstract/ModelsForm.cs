﻿using CarRoomCW.Data;
using CarRoomCW.Data.Abstract;
using CarRoomCW.Data.Edit;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRoomCW.Forms.Abstract
{
    public abstract partial class ModelsForm<TModel, TEditForm> : Form, IDataForm<TModel> where TModel : ViewModel where TEditForm: Form, IEditForm, new()
    {
        public abstract string CountQuery { get; }
        public abstract string DataQuery { get; }
        public abstract Size GridSize { get; }
        public abstract Size CellSize { get; }
        public abstract Size FormSize { get; }
        public virtual string PageText => "Страница {0} из {1}";
        public abstract string FormText { get; }
        public abstract string SelectModelTitle { get; }
        public abstract string SelectModelText { get; }
        public abstract string RemoveConfirmTitle { get; }
        public abstract string RemoveConfirmText { get; }

        public int Page { get; set; } = 1;
        public int TotalPages { get; set; }
        public List<TModel> Models { get; } = new List<TModel>();
        public TModel ActiveModel { get; set; }
        public ISelectionForm SelectionForm { get; set; }

        public abstract Type ModelType { get; }

        public ModelsForm()
        {
            InitializeComponent();
        }

        protected async void ModelsForm_Load(object sender, EventArgs e)
        {
            await ResetPages();
            await ResetData();
        }

        public abstract Control[] BuildInner(TModel model);

        public Panel BuildPanel(TModel model)
        {
            var panel = new Panel
            {
                BorderStyle = BorderStyle.FixedSingle,
                Size = new Size(CellSize.Width - 4, CellSize.Height - 4),
                Cursor = Cursors.Hand,
            };

            model.Control = panel;

            panel.Controls.AddRange(BuildInner(model));

            void click(object sender, EventArgs args) => ActiveModel = model;
            panel.Click += click;
            foreach (Control control in panel.Controls)
                control.Click += click;

            panel.BorderStyle = BorderStyle.None;
            panel.Paint += (o, e) => ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, Color.LightGray, ButtonBorderStyle.Solid);

            return panel;
        }

        public async Task ResetPages()
        {
            var total = (int)await new DbContext().Command(CountQuery).ScalarAsync();
            TotalPages = (int)Math.Ceiling((float)total / (GridSize.Width * GridSize.Height));
            labelPage.Text = string.Format(PageText, Page, TotalPages);
        }

        protected abstract TModel CreateModel(SqlDataReader reader);

        public async Task ResetData()
        {
            SuspendLayout();

            groupBoxData.Controls.Clear();
            using (var reader = await new DbContext().Command(DataQuery).UseParams((Page - 1) * GridSize.Width * GridSize.Height, GridSize.Width * GridSize.Height).ReaderAsync())
            {
                for (var i = 0; i < GridSize.Height; i++)
                    for (var j = 0; j < GridSize.Width; j++)
                    {
                        if (!await reader.ReadAsync())
                            break;

                        var model = CreateModel(reader);
                        Models.Add(model);

                        var panel = BuildPanel(model);
                        panel.Location = new Point(5 + j * CellSize.Width, 24 + i * CellSize.Height);
                        groupBoxData.Controls.Add(panel);
                    }
            }

            ResumeLayout();
        }

        protected async void ButtonPrev_Click(object sender, EventArgs e)
        {
            Page--;
            if (Page < 1)
                Page = 1;
            await ResetPages();
            await ResetData();
        }

        protected async void ButtonNext_Click(object sender, EventArgs e)
        {
            Page++;
            if (Page > TotalPages)
                Page = TotalPages;
            await ResetPages();
            await ResetData();
        }

        protected async void ButtonRemove_Click(object sender, EventArgs e)
        {
            // TODO : Check can remove (there is no rows in connected tables)

            if (!CheckSelected())
                return;

            var confirm = MessageBox.Show(RemoveConfirmText, RemoveConfirmTitle, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (confirm == DialogResult.Cancel)
                return;

            using (var remover = new SingleRemover())
                await remover.ByType(ActiveModel.Id, ModelType);
            await ResetPages();
            await ResetData();
        }

        protected bool CheckSelected()
        {
            if (ActiveModel == null)
            {
                MessageBox.Show(SelectModelText, SelectModelTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return ActiveModel != null;
        }

        protected async void ButtonEdit_Click(object sender, EventArgs e)
        {
            var form = new TEditForm();
            form.DataForm = this;
            if (!CheckSelected())
                return;
            await form.SetEdit(ActiveModel.Id);
            form.Show();
        }
        
        protected async void ButtonAdd_Click(object sender, EventArgs e)
        {
            var form = new TEditForm();
            form.DataForm = this;
            await form.SetAdding();
            form.Show();
        }

        protected async void ButtonSelect_Click(object sender, EventArgs e)
        {
            if (!CheckSelected())
                return;
            await SelectionForm.Select(ActiveModel.Id, ModelType);
            Close();
        }
    }
}
