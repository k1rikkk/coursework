﻿using System;
using System.Threading.Tasks;

namespace CarRoomCW.Forms.Abstract
{
    public interface IEditForm
    {
        Task SetEdit(Guid id);
        Task SetAdding();
        Guid? ModelId { get; set; }
        IDataForm DataForm { get; set; }
    }
}
