﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRoomCW.Forms.Abstract
{
    public class EditForm : Form, IEditForm
    {
        public virtual Guid? ModelId { get; set; }
        public virtual IDataForm DataForm { get; set; }

        public virtual Task SetAdding() => throw new NotImplementedException();
        public virtual Task SetEdit(Guid id) => throw new NotImplementedException();

        public virtual Task Save() => throw new NotImplementedException();
        public virtual Task RemoveSingle() => throw new NotImplementedException();
        
        public virtual async Task Remove()
        {
            if (!ModelId.HasValue)
            {
                Close();
                return;
            }

            var confirm = MessageBox.Show("Вы уверены, что хотите удалить клиента?", "Удаление клиента", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (confirm == DialogResult.Cancel)
                return;

            await RemoveSingle();
            Close();
            if (DataForm != null)
            {
                await DataForm.ResetPages();
                await DataForm.ResetData();
            }
        }
    }
}
