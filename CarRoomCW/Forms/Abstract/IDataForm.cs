﻿using CarRoomCW.Data.Abstract;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRoomCW.Forms.Abstract
{
    public interface IDataForm
    {
        Task ResetPages();
        Task ResetData();
        string CountQuery { get; }
    }

    public interface IDataForm<TModel> : IDataForm where TModel: ViewModel
    {
        Size GridSize { get; }
        Size CellSize { get; }
        string PageText { get; }

        int Page { get; set; }
        int TotalPages { get; set; }
        List<TModel> Models { get; }
        TModel ActiveModel { get; set; }

        Panel BuildPanel(TModel model);
    }
}
