﻿using System;
using System.Threading.Tasks;

namespace CarRoomCW.Forms.Abstract
{
    public interface ISelectionForm
    {
        Task Select(Guid? model, Type type);
    }
}
