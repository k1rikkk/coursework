﻿namespace CarRoomCW.Forms.Abstract
{
    public partial class ModelsForm<TModel, TEditForm>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        protected System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected virtual void InitializeComponent()
        {
            this.groupBoxData = new System.Windows.Forms.GroupBox();
            this.groupBoxControls = new System.Windows.Forms.GroupBox();
            this.labelPage = new System.Windows.Forms.Label();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.groupBoxControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxData
            // 
            this.groupBoxData.Location = new System.Drawing.Point(12, 12);
            this.groupBoxData.Name = "groupBoxData";
            this.groupBoxData.Size = new System.Drawing.Size(FormSize.Width - 292, FormSize.Height - 16);
            this.groupBoxData.TabIndex = 0;
            this.groupBoxData.TabStop = false;
            this.groupBoxData.Text = "Данные";
            // 
            // groupBoxControls
            // 
            this.groupBoxControls.Controls.Add(this.labelPage);
            this.groupBoxControls.Controls.Add(this.buttonNext);
            this.groupBoxControls.Controls.Add(this.buttonPrev);
            this.groupBoxControls.Controls.Add(this.buttonRemove);
            this.groupBoxControls.Controls.Add(this.buttonEdit);
            this.groupBoxControls.Controls.Add(this.buttonAdd);
            this.groupBoxControls.Controls.Add(this.buttonSelect);
            this.groupBoxControls.Location = new System.Drawing.Point(FormSize.Width - 269, 12);
            this.groupBoxControls.Name = "groupBoxControls";
            this.groupBoxControls.Size = new System.Drawing.Size(263, FormSize.Height - 16);
            this.groupBoxControls.TabIndex = 2;
            this.groupBoxControls.TabStop = false;
            this.groupBoxControls.Text = "Управление";
            // 
            // labelPage
            // 
            this.labelPage.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            this.labelPage.Location = new System.Drawing.Point(53, groupBoxControls.Height - 35);
            this.labelPage.Name = "labelPage";
            this.labelPage.Size = new System.Drawing.Size(158, 20);
            this.labelPage.TabIndex = 5;
            this.labelPage.Text = "Страница 1 из 1";
            this.labelPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            this.buttonNext.Location = new System.Drawing.Point(217, groupBoxControls.Height - 43);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(40, 37);
            this.buttonNext.TabIndex = 4;
            this.buttonNext.Text = "-->";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(ButtonNext_Click);
            // 
            // buttonPrev
            // 
            this.buttonNext.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            this.buttonPrev.Location = new System.Drawing.Point(6, groupBoxControls.Height - 43);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(41, 37);
            this.buttonPrev.TabIndex = 3;
            this.buttonPrev.Text = "<--";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(ButtonPrev_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(6, 111);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(251, 37);
            this.buttonRemove.TabIndex = 2;
            this.buttonRemove.Text = "Удалить";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(ButtonRemove_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Location = new System.Drawing.Point(6, 68);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(251, 37);
            this.buttonEdit.TabIndex = 1;
            this.buttonEdit.Text = "Изменить";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(ButtonEdit_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(6, 25);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(251, 37);
            this.buttonAdd.TabIndex = 0;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(ButtonAdd_Click);
            //
            // buttonSelect
            //
            this.buttonSelect.Location = new System.Drawing.Point(6, 200);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(251, 37);
            this.buttonSelect.TabIndex = 5;
            this.buttonSelect.Text = "Выбрать";
            this.buttonSelect.UseVisualStyleBackColor = true;
            this.buttonSelect.Click += new System.EventHandler(ButtonSelect_Click);
            // 
            // ModelsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = FormSize;
            this.Controls.Add(this.groupBoxControls);
            this.Controls.Add(this.groupBoxData);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ModelsForm";
            this.Text = FormText;
            this.Load += new System.EventHandler(ModelsForm_Load);
            this.groupBoxControls.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.GroupBox groupBoxData;
        protected System.Windows.Forms.GroupBox groupBoxControls;
        protected System.Windows.Forms.Label labelPage;
        protected System.Windows.Forms.Button buttonNext;
        protected System.Windows.Forms.Button buttonPrev;
        protected System.Windows.Forms.Button buttonRemove;
        protected System.Windows.Forms.Button buttonEdit;
        protected System.Windows.Forms.Button buttonAdd;
        protected System.Windows.Forms.Button buttonSelect;
    }
}