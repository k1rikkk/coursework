﻿using CarRoomCW.Data.Edit;
using CarRoomCW.Data.Models;
using CarRoomCW.Data.Select;
using CarRoomCW.Forms.Abstract;
using CarRoomCW.Forms.Cars;
using CarRoomCW.Forms.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRoomCW.Forms.Sells
{
    public partial class EditSellForm : EditForm, ISelectionForm
    {
        private Client client;
        private Car car;

        public EditSellForm()
        {
            InitializeComponent();
        }

        public EditSellForm(IDataForm dataForm) : this() => DataForm = dataForm;

        private async void ButtonSave_Click(object sender, EventArgs e) => await Save();

        private async void ButtonRemove_Click(object sender, EventArgs e) => await Remove();

        private void ButtonCancel_Click(object sender, EventArgs e) => Close();

        public override async Task SetAdding() => await Task.CompletedTask;

        public override async Task SetEdit(Guid id)
        {
            ModelId = id;

            Sell sell = null;
            using (var finder = new SingleFinder())
            {
                sell = await finder.Sell(id);
                client = await finder.Client(sell.ClientId);
                car = await finder.Car(sell.CarId);
                car.Model = await finder.Model(car.ModelId);
                car.Model.Brand = await finder.Brand(car.Model.BrandId);
            }

            textBoxCar.Text = car.Model.Brand.Title + " " + car.Model.Title + " (" + car.LicenseCode + ")";
            textBoxClient.Text = client.LastName + " " + client.FirstName + " " + client.BirthDate.ToString("dd.MM.yyyy");
            textBoxCost.Text = sell.Price.ToString("N0");
            textBoxDate.Text = sell.SoldOn.ToString("dd.MM.yyyy");
            textBoxInfo.Text = sell.Info;
        }

        public override async Task Save()
        {
            if (car == null)
            {
                MessageBox.Show("Выберите автомобиль", "Неверный формат", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxCar.Focus();
                return;
            }
            if (client == null)
            {
                MessageBox.Show("Выберите клиента", "Неверный формат", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxClient.Focus();
                return;
            }
            if (!decimal.TryParse(textBoxCost.Text.Replace(" ", "").Replace(((char)160).ToString(), ""), out var price))
            {
                MessageBox.Show("Цена должна быть вещественным числом с запятой", "Неверный формат", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxCost.Focus();
                return;
            }
            if (!DateTime.TryParse(textBoxDate.Text, out var date))
            {
                MessageBox.Show("Введите дату в формате: <две цифры дня>.<две цифры месяца>.<четыре цифры года>", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxDate.Focus();
                return;
            }
            if (!ModelId.HasValue)
            {
                using (var inserter = new SingleInserter())
                    await inserter.Insert(new Sell()
                    {
                        Id = Guid.NewGuid(),
                        CarId = car.Id.Value,
                        ClientId = client.Id.Value,
                        Info = textBoxInfo.Text,
                        Price = price,
                        SoldOn = date
                    });
            }
            else
            {
                using (var updater = new SingleUpdater())
                    await updater.Sell(new Sell()
                    {
                        Id = ModelId,
                        CarId = car.Id.Value,
                        ClientId = client.Id.Value,
                        Info = textBoxInfo.Text,
                        Price = price,
                        SoldOn = date
                    });
            }
            Close();
            if (DataForm != null)
            {
                await DataForm.ResetPages();
                await DataForm.ResetData();
            }
        }

        public override async Task RemoveSingle()
        {
            using (var remover = new SingleRemover())
                await remover.Sell(ModelId.Value);
        }

        private void TextBoxCar_Click(object sender, EventArgs e)
        {
            var form = new CarsForm();
            form.SelectionForm = this;
            form.Show();
        }

        public async Task Select(Guid? id, Type type)
        {
            using (var finder = new SingleFinder())
            {
                if (type == typeof(Car))
                {
                    car = await finder.Car(id.Value);
                    car.Model = await finder.Model(car.ModelId);
                    car.Model.Brand = await finder.Brand(car.Model.BrandId);
                    textBoxCar.Text = car.Model.Brand.Title + " " + car.Model.Title + " (" + car.LicenseCode + ")";
                }
                else if (type == typeof(Client))
                {
                    client = await finder.Client(id.Value);
                    textBoxClient.Text = client.LastName + " " + client.FirstName + " " + client.BirthDate.ToString("dd.MM.yyyy");
                }
            }
        }

        private void TextBoxClient_Click(object sender, EventArgs e)
        {
            var form = new ClientsForm();
            form.SelectionForm = this;
            form.Show();
        }
    }
}
