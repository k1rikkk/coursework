﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using CarRoomCW.Data.Queries;
using CarRoomCW.Data.ViewModels;
using CarRoomCW.Forms.Abstract;

namespace CarRoomCW.Forms.Sells
{
    public class SellsForm : ModelsForm<Sell, EditSellForm>
    {
        public override string CountQuery => SqlQueries.Instance.Select.SellsCount;
        public override string DataQuery => SqlQueries.Instance.Select.SellsFormQuery;
        public override Size GridSize => new Size(3, 6);
        public override Size CellSize => new Size(277, 85);
        public override Size FormSize => new Size(1130, 552);
        public override string FormText => "Продажи";
        public override Type ModelType => typeof(Data.Models.Sell);

        public override string SelectModelTitle => "Продажа не выбрана";
        public override string SelectModelText => "Выберите продажу";
        public override string RemoveConfirmTitle => "Удаление продажи";
        public override string RemoveConfirmText => "Вы уверены, что хотите удалить продажу";

        public override Control[] BuildInner(Sell model) => new Control[]
        {
            new Label()
            {
                Text = model.Car,
                Location = new Point(3, 27),
                Font = new Font("Microsoft Sans Serif", 14),
                Size = new Size(269, 22)
            },
            new Label()
            {
                Text = model.Client,
                Location = new Point(3, 4),
                Font = new Font("Microsoft Sans Serif", 14),
                Size = new Size(269, 22)
            },
            new Label()
            {
                Text = model.Cost,
                Location = new Point(3, 54),
                Font = new Font("Microsoft Sans Serif", 12),
                Size = new Size(110, 22)
            },
            new Label()
            {
                Text = model.SoldOn,
                Location = new Point(179, 54),
                Font = new Font("Microsoft Sans Serif", 12),
                Size = new Size(90, 22)
            }
        };

        protected override Sell CreateModel(SqlDataReader reader) => new Sell()
        {
            Id = reader.GetGuid(0),
            Car = reader.GetString(1),
            Client = reader.GetString(2),
            Cost = reader.GetDecimal(3).ToString("n") + " Br",
            SoldOn = reader.GetDateTime(4).ToString("dd.MM.yyyy")
        };
    }
}
