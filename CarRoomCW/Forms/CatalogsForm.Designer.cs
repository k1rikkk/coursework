﻿namespace CarRoomCW.Forms
{
    partial class CatalogsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxBrands = new System.Windows.Forms.ListBox();
            this.buttonEditBrand = new System.Windows.Forms.Button();
            this.buttonRemoveBrand = new System.Windows.Forms.Button();
            this.groupBoxBrands = new System.Windows.Forms.GroupBox();
            this.buttonSaveBrand = new System.Windows.Forms.Button();
            this.textBoxBrand = new System.Windows.Forms.TextBox();
            this.groupBoxModels = new System.Windows.Forms.GroupBox();
            this.buttonSaveModel = new System.Windows.Forms.Button();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.listBoxModels = new System.Windows.Forms.ListBox();
            this.buttonRemoveModel = new System.Windows.Forms.Button();
            this.buttonEditModel = new System.Windows.Forms.Button();
            this.groupBoxFeatures = new System.Windows.Forms.GroupBox();
            this.buttonSaveFeature = new System.Windows.Forms.Button();
            this.textBoxFeature = new System.Windows.Forms.TextBox();
            this.listBoxFeatures = new System.Windows.Forms.ListBox();
            this.buttonRemoveFeature = new System.Windows.Forms.Button();
            this.buttonEditFeature = new System.Windows.Forms.Button();
            this.groupBoxBrands.SuspendLayout();
            this.groupBoxModels.SuspendLayout();
            this.groupBoxFeatures.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxBrands
            // 
            this.listBoxBrands.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxBrands.FormattingEnabled = true;
            this.listBoxBrands.ItemHeight = 20;
            this.listBoxBrands.Location = new System.Drawing.Point(6, 25);
            this.listBoxBrands.Name = "listBoxBrands";
            this.listBoxBrands.Size = new System.Drawing.Size(200, 204);
            this.listBoxBrands.TabIndex = 1;
            this.listBoxBrands.SelectedIndexChanged += new System.EventHandler(this.ListBoxBrands_SelectedIndexChanged);
            // 
            // buttonEditBrand
            // 
            this.buttonEditBrand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonEditBrand.Location = new System.Drawing.Point(6, 235);
            this.buttonEditBrand.Name = "buttonEditBrand";
            this.buttonEditBrand.Size = new System.Drawing.Size(97, 31);
            this.buttonEditBrand.TabIndex = 2;
            this.buttonEditBrand.Text = "Изменить";
            this.buttonEditBrand.UseVisualStyleBackColor = true;
            this.buttonEditBrand.Click += new System.EventHandler(this.ButtonEditBrand_Click);
            // 
            // buttonRemoveBrand
            // 
            this.buttonRemoveBrand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRemoveBrand.Location = new System.Drawing.Point(109, 235);
            this.buttonRemoveBrand.Name = "buttonRemoveBrand";
            this.buttonRemoveBrand.Size = new System.Drawing.Size(97, 31);
            this.buttonRemoveBrand.TabIndex = 3;
            this.buttonRemoveBrand.Text = "Удалить";
            this.buttonRemoveBrand.UseVisualStyleBackColor = true;
            this.buttonRemoveBrand.Click += new System.EventHandler(this.ButtonRemoveBrand_Click);
            // 
            // groupBoxBrands
            // 
            this.groupBoxBrands.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxBrands.Controls.Add(this.buttonSaveBrand);
            this.groupBoxBrands.Controls.Add(this.textBoxBrand);
            this.groupBoxBrands.Controls.Add(this.listBoxBrands);
            this.groupBoxBrands.Controls.Add(this.buttonRemoveBrand);
            this.groupBoxBrands.Controls.Add(this.buttonEditBrand);
            this.groupBoxBrands.Location = new System.Drawing.Point(12, 9);
            this.groupBoxBrands.Name = "groupBoxBrands";
            this.groupBoxBrands.Size = new System.Drawing.Size(212, 341);
            this.groupBoxBrands.TabIndex = 4;
            this.groupBoxBrands.TabStop = false;
            this.groupBoxBrands.Text = "Марки";
            // 
            // buttonSaveBrand
            // 
            this.buttonSaveBrand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSaveBrand.Location = new System.Drawing.Point(6, 304);
            this.buttonSaveBrand.Name = "buttonSaveBrand";
            this.buttonSaveBrand.Size = new System.Drawing.Size(200, 31);
            this.buttonSaveBrand.TabIndex = 6;
            this.buttonSaveBrand.Text = "Сохранить";
            this.buttonSaveBrand.UseVisualStyleBackColor = true;
            this.buttonSaveBrand.Click += new System.EventHandler(this.ButtonSaveBrand_Click);
            // 
            // textBoxBrand
            // 
            this.textBoxBrand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxBrand.Location = new System.Drawing.Point(6, 272);
            this.textBoxBrand.Name = "textBoxBrand";
            this.textBoxBrand.Size = new System.Drawing.Size(200, 26);
            this.textBoxBrand.TabIndex = 5;
            // 
            // groupBoxModels
            // 
            this.groupBoxModels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxModels.Controls.Add(this.buttonSaveModel);
            this.groupBoxModels.Controls.Add(this.textBoxModel);
            this.groupBoxModels.Controls.Add(this.listBoxModels);
            this.groupBoxModels.Controls.Add(this.buttonRemoveModel);
            this.groupBoxModels.Controls.Add(this.buttonEditModel);
            this.groupBoxModels.Location = new System.Drawing.Point(230, 9);
            this.groupBoxModels.Name = "groupBoxModels";
            this.groupBoxModels.Size = new System.Drawing.Size(212, 341);
            this.groupBoxModels.TabIndex = 7;
            this.groupBoxModels.TabStop = false;
            this.groupBoxModels.Text = "Модели";
            // 
            // buttonSaveModel
            // 
            this.buttonSaveModel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSaveModel.Location = new System.Drawing.Point(6, 304);
            this.buttonSaveModel.Name = "buttonSaveModel";
            this.buttonSaveModel.Size = new System.Drawing.Size(200, 31);
            this.buttonSaveModel.TabIndex = 6;
            this.buttonSaveModel.Text = "Сохранить";
            this.buttonSaveModel.UseVisualStyleBackColor = true;
            this.buttonSaveModel.Click += new System.EventHandler(this.ButtonSaveModel_Click);
            // 
            // textBoxModel
            // 
            this.textBoxModel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxModel.Location = new System.Drawing.Point(6, 272);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(200, 26);
            this.textBoxModel.TabIndex = 5;
            // 
            // listBoxModels
            // 
            this.listBoxModels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxModels.FormattingEnabled = true;
            this.listBoxModels.ItemHeight = 20;
            this.listBoxModels.Location = new System.Drawing.Point(6, 25);
            this.listBoxModels.Name = "listBoxModels";
            this.listBoxModels.Size = new System.Drawing.Size(200, 204);
            this.listBoxModels.TabIndex = 1;
            // 
            // buttonRemoveModel
            // 
            this.buttonRemoveModel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRemoveModel.Location = new System.Drawing.Point(109, 235);
            this.buttonRemoveModel.Name = "buttonRemoveModel";
            this.buttonRemoveModel.Size = new System.Drawing.Size(97, 31);
            this.buttonRemoveModel.TabIndex = 3;
            this.buttonRemoveModel.Text = "Удалить";
            this.buttonRemoveModel.UseVisualStyleBackColor = true;
            this.buttonRemoveModel.Click += new System.EventHandler(this.ButtonRemoveModel_Click);
            // 
            // buttonEditModel
            // 
            this.buttonEditModel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonEditModel.Location = new System.Drawing.Point(6, 235);
            this.buttonEditModel.Name = "buttonEditModel";
            this.buttonEditModel.Size = new System.Drawing.Size(97, 31);
            this.buttonEditModel.TabIndex = 2;
            this.buttonEditModel.Text = "Изменить";
            this.buttonEditModel.UseVisualStyleBackColor = true;
            this.buttonEditModel.Click += new System.EventHandler(this.ButtonEditModel_Click);
            // 
            // groupBoxFeatures
            // 
            this.groupBoxFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFeatures.Controls.Add(this.buttonSaveFeature);
            this.groupBoxFeatures.Controls.Add(this.textBoxFeature);
            this.groupBoxFeatures.Controls.Add(this.listBoxFeatures);
            this.groupBoxFeatures.Controls.Add(this.buttonRemoveFeature);
            this.groupBoxFeatures.Controls.Add(this.buttonEditFeature);
            this.groupBoxFeatures.Location = new System.Drawing.Point(448, 9);
            this.groupBoxFeatures.Name = "groupBoxFeatures";
            this.groupBoxFeatures.Size = new System.Drawing.Size(212, 341);
            this.groupBoxFeatures.TabIndex = 8;
            this.groupBoxFeatures.TabStop = false;
            this.groupBoxFeatures.Text = "Опции";
            // 
            // buttonSaveFeature
            // 
            this.buttonSaveFeature.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveFeature.Location = new System.Drawing.Point(6, 304);
            this.buttonSaveFeature.Name = "buttonSaveFeature";
            this.buttonSaveFeature.Size = new System.Drawing.Size(200, 31);
            this.buttonSaveFeature.TabIndex = 6;
            this.buttonSaveFeature.Text = "Сохранить";
            this.buttonSaveFeature.UseVisualStyleBackColor = true;
            this.buttonSaveFeature.Click += new System.EventHandler(this.ButtonSaveFeature_Click);
            // 
            // textBoxFeature
            // 
            this.textBoxFeature.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFeature.Location = new System.Drawing.Point(6, 272);
            this.textBoxFeature.Name = "textBoxFeature";
            this.textBoxFeature.Size = new System.Drawing.Size(200, 26);
            this.textBoxFeature.TabIndex = 5;
            // 
            // listBoxFeatures
            // 
            this.listBoxFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxFeatures.FormattingEnabled = true;
            this.listBoxFeatures.ItemHeight = 20;
            this.listBoxFeatures.Location = new System.Drawing.Point(6, 25);
            this.listBoxFeatures.Name = "listBoxFeatures";
            this.listBoxFeatures.Size = new System.Drawing.Size(200, 204);
            this.listBoxFeatures.TabIndex = 1;
            // 
            // buttonRemoveFeature
            // 
            this.buttonRemoveFeature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemoveFeature.Location = new System.Drawing.Point(109, 235);
            this.buttonRemoveFeature.Name = "buttonRemoveFeature";
            this.buttonRemoveFeature.Size = new System.Drawing.Size(97, 31);
            this.buttonRemoveFeature.TabIndex = 3;
            this.buttonRemoveFeature.Text = "Удалить";
            this.buttonRemoveFeature.UseVisualStyleBackColor = true;
            this.buttonRemoveFeature.Click += new System.EventHandler(this.ButtonRemoveFeature_Click);
            // 
            // buttonEditFeature
            // 
            this.buttonEditFeature.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditFeature.Location = new System.Drawing.Point(6, 235);
            this.buttonEditFeature.Name = "buttonEditFeature";
            this.buttonEditFeature.Size = new System.Drawing.Size(97, 31);
            this.buttonEditFeature.TabIndex = 2;
            this.buttonEditFeature.Text = "Изменить";
            this.buttonEditFeature.UseVisualStyleBackColor = true;
            this.buttonEditFeature.Click += new System.EventHandler(this.ButtonEditFeature_Click);
            // 
            // CatalogsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 361);
            this.Controls.Add(this.groupBoxFeatures);
            this.Controls.Add(this.groupBoxModels);
            this.Controls.Add(this.groupBoxBrands);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "CatalogsForm";
            this.Text = "Справочники";
            this.Load += new System.EventHandler(this.CatalogsForm_Load);
            this.groupBoxBrands.ResumeLayout(false);
            this.groupBoxBrands.PerformLayout();
            this.groupBoxModels.ResumeLayout(false);
            this.groupBoxModels.PerformLayout();
            this.groupBoxFeatures.ResumeLayout(false);
            this.groupBoxFeatures.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxBrands;
        private System.Windows.Forms.Button buttonEditBrand;
        private System.Windows.Forms.Button buttonRemoveBrand;
        private System.Windows.Forms.GroupBox groupBoxBrands;
        private System.Windows.Forms.Button buttonSaveBrand;
        private System.Windows.Forms.TextBox textBoxBrand;
        private System.Windows.Forms.GroupBox groupBoxModels;
        private System.Windows.Forms.Button buttonSaveModel;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.ListBox listBoxModels;
        private System.Windows.Forms.Button buttonRemoveModel;
        private System.Windows.Forms.Button buttonEditModel;
        private System.Windows.Forms.GroupBox groupBoxFeatures;
        private System.Windows.Forms.Button buttonSaveFeature;
        private System.Windows.Forms.TextBox textBoxFeature;
        private System.Windows.Forms.ListBox listBoxFeatures;
        private System.Windows.Forms.Button buttonRemoveFeature;
        private System.Windows.Forms.Button buttonEditFeature;
    }
}