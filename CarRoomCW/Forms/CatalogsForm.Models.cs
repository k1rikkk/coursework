﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CarRoomCW.Data.Select;
using CarRoomCW.Data.Models;
using CarRoomCW.Data.Edit;

namespace CarRoomCW.Forms
{
    public partial class CatalogsForm
    {
        private Model activeModel;

		public async Task ResetModels(Guid brandId)
        {
            listBoxModels.Items.Clear();
            using (var finder = new MiscFinder())
            {
                var models = await finder.BrandModels(brandId);
                listBoxModels.Items.AddRange(models.ToArray());
            }
        }

        private void ButtonEditModel_Click(object sender, EventArgs e)
        {
            activeModel = (Model)listBoxModels.SelectedItem;
            textBoxModel.Text = activeModel.Title;
        }

        private async void ButtonRemoveModel_Click(object sender, EventArgs e) => await RemoveModel(typeof(Model), ((Model)listBoxModels.SelectedItem).Id.Value, "Вы уверены, что хотите удалить модель?", "Удаление модели", () => ResetModels(SelectedBrand.Id.Value));

        private async void ButtonSaveModel_Click(object sender, EventArgs e)
        {
            if (activeModel != null)
            {
                activeModel.Title = textBoxModel.Text;
                using (var updater = new SingleUpdater())
                    await updater.Model(activeModel);
                activeModel = null;
                textBoxModel.Text = "";
            }
            else
            {
                var model = new Model
                {
                    Id = Guid.NewGuid(),
                    Title = textBoxModel.Text,
                    ComfortClass = "Unset",
                    BrandId = SelectedBrand.Id.Value
                };
                using (var inserter = new SingleInserter())
                    await inserter.Insert(model);
                textBoxModel.Text = "";
            }
            await ResetModels(SelectedBrand.Id.Value);
        }
    }
}
