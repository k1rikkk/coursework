﻿using System;
using System.Windows.Forms;

namespace CarRoomCW.Forms.Reports
{
    public partial class SelectYearForm : Form
    {
        public int Year { get; set; }

        public SelectYearForm()
        {
            InitializeComponent();
        }

        private void SelectDateSpanForm_Load(object sender, EventArgs e)
        {
            textBoxYear.Text = DateTime.Now.Year.ToString();
        }

        private void ButtonOk_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(textBoxYear.Text, out var year))
            {
                MessageBox.Show("Год должен быть целым числом", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxYear.Focus();
                return;
            }
            Year = year;
            DialogResult = DialogResult.OK;
        }
    }
}
