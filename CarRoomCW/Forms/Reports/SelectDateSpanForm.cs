﻿using System;
using System.Windows.Forms;

namespace CarRoomCW.Forms.Reports
{
    public partial class SelectDateSpanForm : Form
    {
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }

        public SelectDateSpanForm()
        {
            InitializeComponent();
        }

        private void SelectDateSpanForm_Load(object sender, EventArgs e)
        {
            textBoxBegin.Text = new DateTime(DateTime.Now.Year, 1, 1).ToString("dd.MM.yyyy");
            textBoxEnd.Text = new DateTime(DateTime.Now.Year, 12, 31).ToString("dd.MM.yyyy");
        }

        private void ButtonOk_Click(object sender, EventArgs e)
        {
            if (!DateTime.TryParse(textBoxBegin.Text, out var begin))
            {
                MessageBox.Show("Введите дату начала в формате: <две цифры дня>.<две цифры месяца>.<четыре цифры года>", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxBegin.Focus();
                return;
            }
            if (!DateTime.TryParse(textBoxEnd.Text, out var end))
            {
                MessageBox.Show("Введите дату окончания в формате: <две цифры дня>.<две цифры месяца>.<четыре цифры года>", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxEnd.Focus();
                return;
            }
            Begin = begin;
            End = end;
            DialogResult = DialogResult.OK;
        }
    }
}
