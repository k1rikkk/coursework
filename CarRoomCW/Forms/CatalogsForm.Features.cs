﻿using CarRoomCW.Data.Edit;
using CarRoomCW.Data.Models;
using System;
using System.Threading.Tasks;

namespace CarRoomCW.Forms
{
    public partial class CatalogsForm
    {
        private Feature activeFeature;

        private async Task ResetFeatures()
        {
            listBoxFeatures.Items.Clear();
            await ResetCatalog(finder => finder.Features(), listBoxFeatures);
        }

        private void ButtonEditFeature_Click(object sender, EventArgs e)
        {
            activeFeature = (Feature)listBoxFeatures.SelectedItem;
            textBoxFeature.Text = activeFeature.Title;
        }

        private async void ButtonRemoveFeature_Click(object sender, EventArgs e) => await RemoveModel(typeof(Feature), ((Feature)listBoxFeatures.SelectedItem).Id.Value, "Вы уверены, что хотите удалить опцию?", "Удаление опции", () => ResetFeatures());

        private async void ButtonSaveFeature_Click(object sender, EventArgs e)
        {
            if (activeFeature != null)
            {
                activeFeature.Title = textBoxFeature.Text;
                using (var updater = new SingleUpdater())
                    await updater.Feature(activeFeature);
                activeFeature = null;
                textBoxFeature.Text = "";
            }
            else
            {
                var feature = new Feature
                {
                    Id = Guid.NewGuid(),
                    Title = textBoxFeature.Text,
                    Info = "Unset"
                };
                using (var inserter = new SingleInserter())
                    await inserter.Insert(feature);
                textBoxFeature.Text = "";
            }
            await ResetFeatures();
        }
    }
}
