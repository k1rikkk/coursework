﻿using CarRoomCW.Data.Edit;
using CarRoomCW.Data.Models;
using System;
using System.Windows.Forms;

namespace CarRoomCW.Forms.Cars
{
    public partial class AddFeatureForm : Form
    {
        public static Feature GetFeature(string title = "")
        {
            var form = new AddFeatureForm();
            form.textBoxTitle.Text = title;
            if (form.ShowDialog() == DialogResult.OK)
                return form.feature;
            else
                return null;
        }

        private Feature feature;

        public AddFeatureForm()
        {
            InitializeComponent();
        }

        private async void ButtonAdd_Click(object sender, EventArgs e)
        {
            feature = new Feature()
            {
                Id = Guid.NewGuid(),
                Title = textBoxTitle.Text,
                Info = textBoxInfo.Text
            };
            using (var inserter = new SingleInserter())
                await inserter.Insert(feature);
            DialogResult = DialogResult.OK;
        }
    }
}
