﻿using CarRoomCW.Forms.Abstract;

namespace CarRoomCW.Forms.Cars
{
    partial class EditCarForm : EditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxBrand = new System.Windows.Forms.ComboBox();
            this.labelBrand = new System.Windows.Forms.Label();
            this.labelModel = new System.Windows.Forms.Label();
            this.comboBoxModel = new System.Windows.Forms.ComboBox();
            this.labelLicense = new System.Windows.Forms.Label();
            this.labelKilometrage = new System.Windows.Forms.Label();
            this.textBoxLicense = new System.Windows.Forms.TextBox();
            this.textBoxKilometrage = new System.Windows.Forms.TextBox();
            this.textBoxDate = new System.Windows.Forms.TextBox();
            this.textBoxCost = new System.Windows.Forms.TextBox();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelCost = new System.Windows.Forms.Label();
            this.labelFeatures = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.labelCur = new System.Windows.Forms.Label();
            this.listBoxFeatures = new System.Windows.Forms.ListBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonRemoveFeature = new System.Windows.Forms.Button();
            this.buttonAddFeature = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBoxBrand
            // 
            this.comboBoxBrand.FormattingEnabled = true;
            this.comboBoxBrand.Location = new System.Drawing.Point(151, 12);
            this.comboBoxBrand.Name = "comboBoxBrand";
            this.comboBoxBrand.Size = new System.Drawing.Size(184, 28);
            this.comboBoxBrand.TabIndex = 0;
            this.comboBoxBrand.TextChanged += new System.EventHandler(this.ComboBoxBrand_TextChanged);
            // 
            // labelBrand
            // 
            this.labelBrand.Location = new System.Drawing.Point(12, 12);
            this.labelBrand.Name = "labelBrand";
            this.labelBrand.Size = new System.Drawing.Size(133, 28);
            this.labelBrand.TabIndex = 1;
            this.labelBrand.Text = "Марка";
            this.labelBrand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelModel
            // 
            this.labelModel.Location = new System.Drawing.Point(12, 45);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(133, 28);
            this.labelModel.TabIndex = 3;
            this.labelModel.Text = "Модель";
            this.labelModel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxModel
            // 
            this.comboBoxModel.FormattingEnabled = true;
            this.comboBoxModel.Location = new System.Drawing.Point(151, 45);
            this.comboBoxModel.Name = "comboBoxModel";
            this.comboBoxModel.Size = new System.Drawing.Size(184, 28);
            this.comboBoxModel.TabIndex = 2;
            this.comboBoxModel.TextChanged += new System.EventHandler(this.ComboBoxModel_TextChanged);
            // 
            // labelLicense
            // 
            this.labelLicense.Location = new System.Drawing.Point(12, 78);
            this.labelLicense.Name = "labelLicense";
            this.labelLicense.Size = new System.Drawing.Size(133, 28);
            this.labelLicense.TabIndex = 5;
            this.labelLicense.Text = "Госномер";
            this.labelLicense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelKilometrage
            // 
            this.labelKilometrage.Location = new System.Drawing.Point(12, 111);
            this.labelKilometrage.Name = "labelKilometrage";
            this.labelKilometrage.Size = new System.Drawing.Size(133, 28);
            this.labelKilometrage.TabIndex = 7;
            this.labelKilometrage.Text = "Пробег";
            this.labelKilometrage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxLicense
            // 
            this.textBoxLicense.Location = new System.Drawing.Point(151, 80);
            this.textBoxLicense.Name = "textBoxLicense";
            this.textBoxLicense.Size = new System.Drawing.Size(184, 26);
            this.textBoxLicense.TabIndex = 8;
            // 
            // textBoxKilometrage
            // 
            this.textBoxKilometrage.Location = new System.Drawing.Point(151, 113);
            this.textBoxKilometrage.Name = "textBoxKilometrage";
            this.textBoxKilometrage.Size = new System.Drawing.Size(184, 26);
            this.textBoxKilometrage.TabIndex = 9;
            // 
            // textBoxDate
            // 
            this.textBoxDate.Location = new System.Drawing.Point(151, 146);
            this.textBoxDate.Name = "textBoxDate";
            this.textBoxDate.Size = new System.Drawing.Size(184, 26);
            this.textBoxDate.TabIndex = 10;
            // 
            // textBoxCost
            // 
            this.textBoxCost.Location = new System.Drawing.Point(151, 179);
            this.textBoxCost.Name = "textBoxCost";
            this.textBoxCost.Size = new System.Drawing.Size(150, 26);
            this.textBoxCost.TabIndex = 11;
            // 
            // labelDate
            // 
            this.labelDate.Location = new System.Drawing.Point(12, 144);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(133, 28);
            this.labelDate.TabIndex = 11;
            this.labelDate.Text = "Дата выпуска";
            this.labelDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCost
            // 
            this.labelCost.Location = new System.Drawing.Point(12, 177);
            this.labelCost.Name = "labelCost";
            this.labelCost.Size = new System.Drawing.Size(88, 28);
            this.labelCost.TabIndex = 10;
            this.labelCost.Text = "РРЦ";
            this.labelCost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFeatures
            // 
            this.labelFeatures.Location = new System.Drawing.Point(346, 11);
            this.labelFeatures.Name = "labelFeatures";
            this.labelFeatures.Size = new System.Drawing.Size(71, 28);
            this.labelFeatures.TabIndex = 14;
            this.labelFeatures.Text = "Опции";
            this.labelFeatures.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCancel.Location = new System.Drawing.Point(460, 216);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(229, 48);
            this.buttonCancel.TabIndex = 17;
            this.buttonCancel.Text = "Отменить";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRemove.Location = new System.Drawing.Point(260, 216);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(194, 48);
            this.buttonRemove.TabIndex = 18;
            this.buttonRemove.Text = "Удалить";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.ButtonRemove_Click);
            // 
            // labelCur
            // 
            this.labelCur.Location = new System.Drawing.Point(307, 178);
            this.labelCur.Name = "labelCur";
            this.labelCur.Size = new System.Drawing.Size(28, 28);
            this.labelCur.TabIndex = 19;
            this.labelCur.Text = "Br";
            this.labelCur.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // listBoxFeatures
            // 
            this.listBoxFeatures.FormattingEnabled = true;
            this.listBoxFeatures.ItemHeight = 20;
            this.listBoxFeatures.Location = new System.Drawing.Point(423, 14);
            this.listBoxFeatures.Name = "listBoxFeatures";
            this.listBoxFeatures.Size = new System.Drawing.Size(266, 184);
            this.listBoxFeatures.TabIndex = 20;
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSave.Location = new System.Drawing.Point(16, 216);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(238, 48);
            this.buttonSave.TabIndex = 16;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // buttonRemoveFeature
            // 
            this.buttonRemoveFeature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRemoveFeature.Location = new System.Drawing.Point(350, 42);
            this.buttonRemoveFeature.Name = "buttonRemoveFeature";
            this.buttonRemoveFeature.Size = new System.Drawing.Size(29, 28);
            this.buttonRemoveFeature.TabIndex = 21;
            this.buttonRemoveFeature.Text = "-";
            this.buttonRemoveFeature.UseVisualStyleBackColor = true;
            this.buttonRemoveFeature.Click += new System.EventHandler(this.ButtonRemoveFeature_Click);
            // 
            // buttonAddFeature
            // 
            this.buttonAddFeature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddFeature.Location = new System.Drawing.Point(385, 42);
            this.buttonAddFeature.Name = "buttonAddFeature";
            this.buttonAddFeature.Size = new System.Drawing.Size(29, 28);
            this.buttonAddFeature.TabIndex = 22;
            this.buttonAddFeature.Text = "+";
            this.buttonAddFeature.UseVisualStyleBackColor = true;
            this.buttonAddFeature.Click += new System.EventHandler(this.ButtonAddFeature_Click);
            // 
            // EditCarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 276);
            this.Controls.Add(this.buttonAddFeature);
            this.Controls.Add(this.buttonRemoveFeature);
            this.Controls.Add(this.listBoxFeatures);
            this.Controls.Add(this.labelCur);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.labelFeatures);
            this.Controls.Add(this.textBoxDate);
            this.Controls.Add(this.textBoxCost);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.labelCost);
            this.Controls.Add(this.textBoxKilometrage);
            this.Controls.Add(this.textBoxLicense);
            this.Controls.Add(this.labelKilometrage);
            this.Controls.Add(this.labelLicense);
            this.Controls.Add(this.labelModel);
            this.Controls.Add(this.comboBoxModel);
            this.Controls.Add(this.labelBrand);
            this.Controls.Add(this.comboBoxBrand);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "EditCarForm";
            this.Text = "AddCarForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxBrand;
        private System.Windows.Forms.Label labelBrand;
        private System.Windows.Forms.Label labelModel;
        private System.Windows.Forms.ComboBox comboBoxModel;
        private System.Windows.Forms.Label labelLicense;
        private System.Windows.Forms.Label labelKilometrage;
        private System.Windows.Forms.TextBox textBoxLicense;
        private System.Windows.Forms.TextBox textBoxKilometrage;
        private System.Windows.Forms.TextBox textBoxDate;
        private System.Windows.Forms.TextBox textBoxCost;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelCost;
        private System.Windows.Forms.Label labelFeatures;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Label labelCur;
        private System.Windows.Forms.ListBox listBoxFeatures;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonRemoveFeature;
        private System.Windows.Forms.Button buttonAddFeature;
    }
}