﻿using CarRoomCW.Data;
using CarRoomCW.Data.Queries;
using CarRoomCW.Data.ViewModels;
using CarRoomCW.Forms.Abstract;
using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace CarRoomCW.Forms.Cars
{
    public class CarsForm : ModelsForm<Car, EditCarForm>
    {
        public override Size GridSize => new Size(3, 4);
        public override Size CellSize => new Size(275, 92);
        public override Size FormSize => new Size(1124, 416);
        public override string FormText => "Автомобили";
        public override string CountQuery => SqlQueries.Instance.Select.CarsCount;
        public override string DataQuery => SqlQueries.Instance.Select.CarsFormQuery;
        public override Type ModelType => typeof(Data.Models.Car);

        public override string SelectModelTitle => "Автомобиль не выбран";
        public override string SelectModelText => "Выберите автомобиль";
        public override string RemoveConfirmTitle => "Удаление автомобиля";
        public override string RemoveConfirmText => "Вы уверены, что хотите удалить автомобиль";

        public CarsForm() : base() { }
        
        public override Control[] BuildInner(Car model) => new Control[]
        {
            new Label()
            {
                Text = model.BrandModel,
                Location = new Point(3, 4),
                Font = new Font("Microsoft Sans Serif", 14),
                Size = new Size(264, 30)
            },
            new Label()
            {
                Text = model.License,
                Location = new Point(3, 34),
                Font = new Font("Microsoft Sans Serif", 12),
                Size = new Size(92, 23)
            },
            new Label()
            {
                Text = FormatKilometrage(model.Kilometrage),
                Location = new Point(101, 34),
                Font = new Font("Microsoft Sans Serif", 12),
                Size = new Size(92, 23)
            },
            new Label()
            {
                Text = model.Date.ToString("MM.yyyy"),
                Location = new Point(199, 34),
                Font = new Font("Microsoft Sans Serif", 12),
                Size = new Size(68, 23)
            },
            new Label()
            {
                Text = model.OwnerCost,
                Location = new Point(3, 57),
                Font = new Font("Microsoft Sans Serif", 12),
                Size = new Size(263, 23)
            }
        };

        protected override Car CreateModel(SqlDataReader reader) => new Data.ViewModels.Car()
        {
            Id = reader.GetGuid(0),
            BrandModel = reader.GetString(1),
            Date = reader.GetDateTime(2),
            Kilometrage = reader.GetInt32(3),
            License = reader.IsDBNull(4) ? "Без г/н" : reader.GetString(4),
            OwnerCost = !reader.IsDBNull(6) ? reader.GetString(6) : reader.GetDecimal(5).ToString("n") + " Br"
        };

        private async void CarsForm_Load(object sender, EventArgs e)
        {
            var total = (int)await new DbContext().Command(SqlQueries.Instance.Select.CarsCount).ScalarAsync();
            TotalPages = (int)Math.Ceiling((float)total / (GridSize.Width * GridSize.Height));

            await ResetPages();
            await ResetData();
        }

        private string FormatKilometrage(int kilometrage) => kilometrage.ToString("n0") + " км";
    }
}
