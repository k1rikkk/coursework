﻿using CarRoomCW.Data.Models;
using CarRoomCW.Data.Select;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRoomCW.Forms.Cars
{
    public partial class SelectFeatureForm : Form
    {
        private IEnumerable<Feature> features;
        private readonly EditCarForm carForm;

        public SelectFeatureForm(EditCarForm carForm)
        {
            this.carForm = carForm;
            InitializeComponent();
        }

        private async Task ResetList()
        {
            using (var finder = new MiscFinder())
                features = await finder.FindFeatures(textBoxQuery.Text);
            listBoxFeatures.Items.Clear();
            listBoxFeatures.Items.Add("Новая опция..");
            listBoxFeatures.Items.AddRange(features.Select(f => f.Title).ToArray());
            listBoxFeatures.SelectedIndex = listBoxFeatures.Items.Count > 1 ? 1 : 0;
        }

        private async void ButtonFind_Click(object sender, EventArgs e) => await ResetList();

        private async void TextBoxQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                await ResetList();
        }

        private async Task Save()
        {
            if (listBoxFeatures.SelectedIndex < 0)
            {
                MessageBox.Show("Выберите опцию", "Опция не выбрана", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else if (listBoxFeatures.SelectedIndex == 0)
            {
                var feature = AddFeatureForm.GetFeature(textBoxQuery.Text);
                if (feature != null)
                {
                    carForm.AddFeature(feature);
                    await ResetList();
                }
            }
            else
                carForm.AddFeature(features.ElementAt(listBoxFeatures.SelectedIndex - 1));
        }

        private async void SelectFeatureForm_Load(object sender, EventArgs e) => await ResetList();

        private async void ButtonOk_Click(object sender, EventArgs e) => await Save();

        private void ListBoxFeatures_DoubleClick(object sender, EventArgs e) => ButtonOk_Click(sender, e);

        private void ButtonCancel_Click(object sender, EventArgs e) => Close();

        private async void ButtonClear_Click(object sender, EventArgs e)
        {
            textBoxQuery.Clear();
            await ResetList();
        }
    }
}
