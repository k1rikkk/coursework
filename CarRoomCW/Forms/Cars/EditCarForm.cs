﻿using CarRoomCW.Data.Edit;
using CarRoomCW.Data.Select;
using CarRoomCW.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CarRoomCW.Forms.Abstract;

namespace CarRoomCW.Forms.Cars
{
    public partial class EditCarForm : EditForm
    {
        private volatile bool fireSelectEvets = true;

        private Brand selectedBrand;
        private Model selectedModel;

        private IEnumerable<Brand> brands = new Brand[] { };
        private IEnumerable<Model> models = new Model[] { };

        private List<Feature> features = new List<Feature>();

        public EditCarForm()
        {
            InitializeComponent();
        }

        public EditCarForm(IDataForm dataForm) : this() => DataForm = dataForm;

        public override async Task SetAdding()
        {
            await AllBrands();
            await InitializeTips();
        }

        public override async Task SetEdit(Guid carId)
        {
            this.ModelId = carId;
            
            Data.Models.Car car = null;

            using (var finder = new SingleFinder())
            {
                car = await finder.Car(carId);
                car.Model = await finder.Model(car.ModelId);
                car.Model.Brand = await finder.Brand(car.Model.BrandId);
            }

            using (var finder = new MiscFinder())
                features = (await finder.CarFeatures(carId)).ToList();

            comboBoxBrand.Text = car.Model.Brand.Title;
            comboBoxModel.Text = car.Model.Title;
            textBoxLicense.Text = car.LicenseCode;
            textBoxCost.Text = car.Price.ToString("n");
            textBoxDate.Text = car.ManufacturedOn.ToString("MM.yyyy");
            textBoxKilometrage.Text = car.Kilometrage.ToString("N0");

            listBoxFeatures.Items.Clear();
            listBoxFeatures.Items.AddRange(features.Select(f => f.Title).ToArray());

            await AllBrands();
            ResetSelectedBrand();
            await InitializeTips();
            ResetSelectedModel();
        }

        private async Task InitializeTips()
        {
            try
            {
                fireSelectEvets = false;

                if (string.IsNullOrWhiteSpace(comboBoxBrand.Text))
                {
                    if (string.IsNullOrWhiteSpace(comboBoxModel.Text))
                    {
                        await AllModels();
                    }
                    else
                    {
                        await BrandByModel();
                        await ModelsByBrand();
                    }
                }
                else
                {
                    await ModelsByBrand();
                }
            }
            finally
            {
                fireSelectEvets = true;
            }
        }

        private async Task BrandByModel()
        {
            using (var finder = new SingleFinder())
            {
                if (selectedModel != null)
                {
                    selectedBrand = await finder.Brand(selectedModel.BrandId);
                    comboBoxBrand.Text = selectedBrand.Title;
                }
            }
        }

        private async Task ModelsByBrand()
        {
            var text = comboBoxModel.Text;
            comboBoxModel.Items.Clear();
            if (selectedBrand != null)
            {
                using (var finder = new MiscFinder())
                {
                    models = await finder.BrandModels(selectedBrand.Id.Value);
                    comboBoxModel.Items.AddRange(models.Select(m => m.Title).ToArray());
                }
            }
            comboBoxModel.Text = text;
        }

        private async Task AllBrands()
        {
            using (var finder = new AllFinder())
            {
                brands = await finder.Brands();
                comboBoxBrand.Items.Clear();
                comboBoxBrand.Items.AddRange(brands.Select(b => b.Title).ToArray());
            }
        }

        private async Task AllModels()
        {
            using (var finder = new AllFinder())
            {
                models = await finder.Models();
                comboBoxModel.Items.Clear();
                comboBoxModel.Items.AddRange(models.Select(m => m.Title).ToArray());
            }
        }

        private void ResetSelectedBrand() => selectedBrand = brands.SingleOrDefault(b => b.Title == comboBoxBrand.Text);

        private void ResetSelectedModel() => selectedModel = models.SingleOrDefault(m => m.Title == comboBoxModel.Text);

        private async void ComboBoxBrand_TextChanged(object sender, EventArgs e)
        {
            if (!fireSelectEvets)
                return;
            ResetSelectedBrand();
            await InitializeTips();
        }

        private async void ComboBoxModel_TextChanged(object sender, EventArgs e)
        {
            var cursorStart = comboBoxModel.SelectionStart;
            var cursorLength = comboBoxModel.SelectionLength;

            if (!fireSelectEvets)
                return;
            ResetSelectedModel();
            await InitializeTips();

             if (selectedModel == null)
                comboBoxModel.Select(cursorStart, cursorLength);
            else
                comboBoxModel.SelectAll();
        }

        public override async Task Save()
        {
            if (string.IsNullOrWhiteSpace(comboBoxBrand.Text))
            {
                MessageBox.Show("Введите марку", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Error);
                comboBoxBrand.Focus();
                return;
            }
            if (string.IsNullOrWhiteSpace(comboBoxModel.Text))
            {
                MessageBox.Show("Введите модель", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Error);
                comboBoxModel.Focus();
                return;
            }
            if (!int.TryParse(textBoxKilometrage.Text.Replace(" ", "").Replace(((char)160).ToString(), ""), out var kilometrage))
            {
                MessageBox.Show("Пробег должен быть целым числом", "Неверный формат", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxKilometrage.Focus();
                return;
            }
            if (!DateTime.TryParse(textBoxDate.Text, out var date))
            {
                MessageBox.Show("Введите дату в формате: <две цифры месяца>.<четыре цифры года>", "Неверный формат", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxDate.Focus();
                return;
            }
            if (!decimal.TryParse(textBoxCost.Text.Replace(" ", "").Replace(((char)160).ToString(), ""), out var price))
            {
                MessageBox.Show("Цена должна быть вещественным числом с запятой", "Неверный формат", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxCost.Focus();
                return;
            }

            using (var insert = new SingleInserter())
            {
                if (selectedBrand == null)
                {
                    selectedBrand = new Brand()
                    {
                        Id = Guid.NewGuid(),
                        Title = comboBoxBrand.Text,
                        Country = Prompt.ShowDialog("Добавление марки", "Введите страну/производитель")
                    };
                    await insert.Insert(selectedBrand);
                }
                if (selectedModel == null)
                {
                    selectedModel = new Model()
                    {
                        Id = Guid.NewGuid(),
                        Title = comboBoxModel.Text,
                        BrandId = selectedBrand.Id.Value,
                        ComfortClass = Prompt.ShowDialog("Добавление модели", "Введите класс модели")
                    };
                    await insert.Insert(selectedModel);
                }
                if (!ModelId.HasValue)
                {
                    ModelId = Guid.NewGuid();
                    await insert.Insert(new Car()
                    {
                        Id = ModelId,
                        Kilometrage = kilometrage,
                        LicenseCode = string.IsNullOrWhiteSpace(textBoxLicense.Text) ? null : textBoxLicense.Text,
                        ManufacturedOn = date,
                        ModelId = selectedModel.Id.Value,
                        Price = price
                    });
                }
                else
                {
                    using (var updater = new SingleUpdater())
                        await updater.Car(new Car()
                        {
                            Id = ModelId,
                            Kilometrage = kilometrage,
                            LicenseCode = string.IsNullOrWhiteSpace(textBoxLicense.Text) ? null : textBoxLicense.Text,
                            ManufacturedOn = date,
                            ModelId = selectedModel.Id.Value,
                            Price = price
                        });
                }
                using (var updater = new MiscUpdater())
                    await updater.UpdateFeatures(ModelId.Value, features);
                Close();
                if (DataForm != null)
                {
                    await DataForm.ResetPages();
                    await DataForm.ResetData();
                }
            }
        }

        private async void ButtonSave_Click(object sender, EventArgs e) => await Save();

        private async void ButtonRemove_Click(object sender, EventArgs e) => await Remove();

        public override async Task RemoveSingle()
        {
            using (var remover = new SingleRemover())
                await remover.Car(ModelId.Value);
        }

        private void ButtonRemoveFeature_Click(object sender, EventArgs e)
        {
            var selected = listBoxFeatures.SelectedIndex;
            if (selected < 0)
                return;
            features.RemoveAt(selected);
            listBoxFeatures.Items.RemoveAt(selected);
        }

        public void AddFeature(Feature feature)
        {
            if (feature == null)
                return;

            var cur = listBoxFeatures.Items.IndexOf(feature.Title);
            if (cur >= 0)
            {
                listBoxFeatures.SelectedIndex = cur;
                return;
            }

            features.Add(feature);
            listBoxFeatures.Items.Add(feature.Title);
            listBoxFeatures.SelectedIndex = listBoxFeatures.Items.Count - 1;
        }

        private void ButtonAddFeature_Click(object sender, EventArgs e) => new SelectFeatureForm(this).Show();

        private void ButtonCancel_Click(object sender, EventArgs e) => Close();
    }
}
