﻿using CarRoomCW.Data.Queries;
using CarRoomCW.Data.ViewModels;
using CarRoomCW.Forms.Abstract;
using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace CarRoomCW.Forms.Clients
{
    public class ClientsForm : ModelsForm<Client, EditClientForm>
    {
        public ClientsForm() : base() { }

        public override Size GridSize => new Size(3, 7);
        public override Size CellSize => new Size(230, 55);
        public override Size FormSize => new Size(989, 435);
        public override string FormText => "Клиенты";
        public override string CountQuery => SqlQueries.Instance.Select.ClientsCount;
        public override string DataQuery => SqlQueries.Instance.Select.ClientsFormQuery;
        public override Type ModelType => typeof(Data.Models.Client);

        public override string SelectModelTitle => "Клиент не выбран";
        public override string SelectModelText => "Выберите клиента";
        public override string RemoveConfirmTitle => "Удаление клиента";
        public override string RemoveConfirmText => "Вы уверены, что хотите удалить клиента";

        public override Control[] BuildInner(Client model) => new Control[]
        {
            new Label()
            {
                Text = model.Name,
                Location = new Point(3, 4),
                Font = new Font("Microsoft Sans Serif", 14),
                Size = new Size(222, 22)
            },
            new Label()
            {
                Text = model.BirthDate,
                Location = new Point(136, 27),
                Font = new Font("Microsoft Sans Serif", 12),
                Size = new Size(89, 21)
            }
        };

        protected override Client CreateModel(SqlDataReader reader) => new Client()
        {
            Id = reader.GetGuid(0),
            Name = reader.GetString(1),
            BirthDate = reader.GetDateTime(2).ToString("dd.MM.yyyy")
        };
    }
}
