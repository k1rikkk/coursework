﻿using CarRoomCW.Data.Edit;
using CarRoomCW.Data.Models;
using CarRoomCW.Data.Select;
using CarRoomCW.Forms.Abstract;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRoomCW.Forms.Clients
{
    public partial class EditClientForm : EditForm
    {
        private DateTime signedOn;

        public EditClientForm()
        {
            InitializeComponent();
        }

        public EditClientForm(IDataForm dataForm) : this() => DataForm = dataForm;

        private async void ButtonSave_Click(object sender, EventArgs e) => await Save();

        private async void ButtonRemove_Click(object sender, EventArgs e) => await Remove();

        private void ButtonCancel_Click(object sender, EventArgs e) => Close();

        public override async Task SetEdit(Guid id)
        {
            ModelId = id;
            Client client = null;

            using (var finder = new SingleFinder())
                client = await finder.Client(id);

            signedOn = client.SignedOn;
            textBoxLastName.Text = client.LastName;
            textBoxFirstName.Text = client.FirstName;
            textBoxBirthDate.Text = client.BirthDate.ToString("dd.MM.yyyy");
        }

        public override async Task SetAdding() => await Task.Run(() => { });

        public override async Task Save()
        {
            if (string.IsNullOrWhiteSpace(textBoxLastName.Text))
            {
                MessageBox.Show("Введите фамилию", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxLastName.Focus();
                return;
            }
            if (string.IsNullOrWhiteSpace(textBoxFirstName.Text))
            {
                MessageBox.Show("Введите имя", "Не введены данные", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxFirstName.Focus();
                return;
            }
            if (!DateTime.TryParse(textBoxBirthDate.Text, out var date))
            {
                MessageBox.Show("Введите дату в формате: <две цифры числа>.<две цифры месяца>.<четыре цифры года>", "Неверный формат", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxBirthDate.Focus();
                return;
            }

            if (!ModelId.HasValue)
            {
                using (var insert = new SingleInserter())
                {
                    await insert.Insert(new Client()
                    {
                        Id = Guid.NewGuid(),
                        SignedOn = DateTime.Now,
                        FirstName = textBoxFirstName.Text,
                        LastName = textBoxLastName.Text,
                        BirthDate = date
                    });
                }
            }
            else
            {
                using (var updater = new SingleUpdater())
                {
                    await updater.Client(new Client()
                    {
                        Id = ModelId.Value,
                        SignedOn = signedOn,
                        FirstName = textBoxFirstName.Text,
                        LastName = textBoxLastName.Text,
                        BirthDate = date
                    });
                }
            }
            Close();
            if (DataForm != null)
            {
                await DataForm.ResetPages();
                await DataForm.ResetData();
            }
        }

        public override async Task RemoveSingle()
        {
            using (var remover = new SingleRemover())
                await remover.Client(ModelId.Value);
        }
    }
}
