﻿using CarRoomCW.Data;
using CarRoomCW.Data.Select;
using CarRoomCW.Data.ViewModels.Reports;
using CarRoomCW.Forms.Cars;
using CarRoomCW.Forms.Clients;
using CarRoomCW.Forms.Reports;
using CarRoomCW.Forms.Sells;
using CarRoomCW.Forms.TestDrives;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRoomCW.Forms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void TimerTime_Tick(object sender, System.EventArgs e)
        {
            labelTime.Text = DateTime.Now.ToString("dd.MM.yyyy hh:mm:ss");
        }

        private void ButtonCars_Click(object sender, EventArgs e) => new CarsForm().Show();

        private async void ButtonAddCar_Click(object sender, EventArgs e)
        {
            var form = new EditCarForm();
            await form.SetAdding();
            form.Show();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            TimerTime_Tick(this, null);
        }

        private void ButtonClients_Click(object sender, EventArgs e) => new ClientsForm().Show();

        private async void ButtonAddClient_Click(object sender, EventArgs e)
        {
            var form = new EditClientForm();
            await form.SetAdding();
            form.Show();
        }

        private async void ButtonAddSell_Click(object sender, EventArgs e)
        {
            var form = new EditSellForm();
            await form.SetAdding();
            form.Show();
        }

        private void ButtonSells_Click(object sender, EventArgs e) => new SellsForm().Show();

        private void ButtonTestDrives_Click(object sender, EventArgs e) => new TestDrivesForm().Show();

        private async void ButtonAddTestDrive_Click(object sender, EventArgs e)
        {
            var form = new EditTestDriveForm();
            await form.SetAdding();
            form.Show();
        }

        private async void ButtonReportCars_Click(object sender, EventArgs e)
        {
            try
            {
                var form = new SelectDateSpanForm();
                if (form.ShowDialog() != DialogResult.OK)
                    return;
                await WriteReport(b => b.Cars(form.Begin, form.End), (r, f) => ReportsWriter.WriteCarReport(f, r));
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }
        }

        private async void ButtonSellsSummary_Click(object sender, EventArgs e)
        {
            try
            {
                var form = new SelectYearForm();
                if (form.ShowDialog() != DialogResult.OK)
                    return;
                await WriteReport(b => b.General(form.Year), (r, f) => ReportsWriter.WriteGeneralReport(f, r));
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }
        }

        private async void ButtonReportSellCars_Click(object sender, EventArgs e)
        {
            try
            {
                await WriteReport(b => b.SellCars(), (r, f) => ReportsWriter.WriteSellCarsReport(f, r));
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }
        }

        private async Task WriteReport<TReport>(Func<ReportsBuilder, Task<TReport>> build, Action<TReport, string> write) where TReport : class
        {
            TReport report = null;
            using (var builder = new ReportsBuilder())
                report = await build(builder);
            var openFile = new SaveFileDialog()
            {
                DefaultExt = "*.docx"
            };
            if (openFile.ShowDialog() != DialogResult.OK)
                return;
            var fileName = openFile.FileName;
            write(report, fileName);
            Process.Start(fileName);
        }

        private void WriteLog(Exception e)
        {
            MessageBox.Show("Внутренняя ошибка. Обратитесь к администратору", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            var text = FormatException(e);
            File.AppendAllText("logs.txt", text);
        }

        private string FormatException(Exception e) => e.Message + "\r\n" + e.StackTrace;

        private void ButtonParents_Click(object sender, EventArgs e) => new CatalogsForm().Show();
    }
}
