﻿using CarRoomCW.Data.Edit;
using CarRoomCW.Data.Models;
using CarRoomCW.Data.Select;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRoomCW.Forms
{
    public partial class CatalogsForm : Form
    {
        public CatalogsForm()
        {
            InitializeComponent();
        }

        private async void CatalogsForm_Load(object sender, EventArgs e)
        {
            await ResetCatalog(finder => finder.Brands(), listBoxBrands);
            if (listBoxBrands.Items.Count > 0)
                listBoxBrands.SelectedIndex = 0;
            await ResetFeatures();
        }

        private async Task ResetCatalog<TModel>(Func<AllFinder, Task<IEnumerable<TModel>>> getAll, ListBox listBox)
        {
            using (var finder = new AllFinder())
            {
                var models = await getAll(finder);
                listBox.Items.AddRange(models.Cast<object>().ToArray());
            }
        }

        private async Task RemoveModel(Type type, Guid id, string removeText, string removeTitle, Func<Task> reset)
        {
            var confirm = MessageBox.Show(removeText, removeTitle, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (confirm == DialogResult.Cancel)
                return;

            using (var remover = new SingleRemover())
                await remover.ByType(id, type);

            await reset();
        }
    }
}
