﻿using System.Windows.Forms;

namespace CarRoomCW.Forms
{
    public static class Prompt
    {
        public static string ShowDialog(string caption, string text, string @default = null)
        {
            var prompt = new Form()
            {
                Width = 314,
                Height = 170,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterParent,
                Font = new System.Drawing.Font("Microsoft Sans Serif", 12)
            };
            var textLabel = new Label()
            {
                Left = 20,
                Top = 10,
                Text = text,
                AutoSize = false,
                Width = 260,
                Height = 30
            };
            var textBox = new TextBox()
            {
                Left = 20,
                Top = 40,
                Width = 260
            };
            var confirmation = new Button()
            {
                Text = "Сохранить",
                Left = 20,
                Width = 120,
                Height = 35,
                Top = 80,
                DialogResult = DialogResult.OK
            };
            var cancellation = new Button()
            {
                Text = "Отмена",
                Left = 160,
                Width = 120,
                Height = 35,
                Top = 80,
                DialogResult = DialogResult.Cancel
            };
            confirmation.Click += (sender, e) => prompt.Close();
            cancellation.Click += (sender, e) => prompt.Close();

            prompt.Controls.AddRange(new Control[] { textBox, confirmation, cancellation, textLabel });
            prompt.AcceptButton = confirmation;

            return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : @default;
        }
    }
}
