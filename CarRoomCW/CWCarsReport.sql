SELECT 
	Brands.Title,
	Models.Title,
	Clients.LastName + ' ' + Clients.FirstName,
	Sells.SoldOn,
	Sells.Price
FROM Sells
	INNER JOIN Cars ON CarId = Cars.Id
	INNER JOIN Clients ON ClientId = Clients.Id
	INNER JOIN Models ON ModelId = Models.Id
	INNER JOIN Brands ON BrandId = Brands.Id
GO


DROP PROC ReportGeneral
GO

DROP PROC TrackQuery
GO

CREATE PROC TrackQuery
AS	
	SET NOCOUNT ON 
	DECLARE @ExecStr varchar(50), @Qry nvarchar(255)
 
	CREATE TABLE #inputbuffer 
	(
		EventType nvarchar(MAX), 
		Parameters int, 
		EventInfo nvarchar(MAX)
	)
 
	SET @ExecStr = 'DBCC INPUTBUFFER(' + STR(@@SPID) + ')'
 
	INSERT INTO #inputbuffer EXEC (@ExecStr)
 
	SET @Qry = (SELECT EventInfo FROM #inputbuffer)

	INSERT INTO ChangeLogs VALUES (NEWID(), CURRENT_TIMESTAMP, @Qry, USER, SYSTEM_USER)
GO

SELECT * FROM Brands

BEGIN TRAN

DROP TABLE Brands
GO

CREATE TABLE Brands (
	Id UNIQUEIDENTIFIER PRIMARY KEY,
	Title NVARCHAR(MAX) NOT NULL,
	Country NVARCHAR(MAX) NOT NULL
)

INSERT INTO Brands VALUES ('DA2E3ABD-9482-4D4A-AD1F-9854EC9139CF', 'Mercedes-benz', 'Германия')
INSERT INTO Brands VALUES ('98191957-914E-475F-BD6A-E8DD7F7EB6A6', 'Audi', 'Германия')

COMMIT

DROP TABLE ChangeLogs
GO

DROP TRIGGER BrandsEdit
GO

CREATE TRIGGER PostsEdit ON Posts FOR INSERT, UPDATE, DELETE
AS
BEGIN
	EXEC TrackQuery
END
GO

SELECT * FROM Posts
SELECT * FROM ChangeLogs


INSERT INTO Posts VALUES ('7ABC362D-BF68-4F37-9B91-E411F423CC14', 'Test', '')
GO

CREATE PROC ReportGeneral @year INT
AS
	CREATE TABLE #report (
		MonthId INT,
		Brand VARCHAR(MAX),
		Model VARCHAR(MAX),
		Client VARCHAR(MAX),
		Price MONEY
	)
	DECLARE @MonthId INT = 1
	WHILE @MonthId <= 12
	BEGIN
		INSERT INTO #report 
			SELECT @MonthId, Brands.Title, Models.Title, Clients.LastName + ' ' + Clients.FirstName, Sells.Price
			FROM Sells 
				INNER JOIN Cars ON CarId = Cars.Id 
				INNER JOIN Models ON ModelId = Models.Id
				INNER JOIN Brands ON BrandId = Brands.Id
				INNER JOIN Clients ON ClientId = Clients.Id
			WHERE YEAR(SoldOn) = @year AND MONTH(SoldOn) = @MonthId
		DECLARE @MonthSum MONEY = (SELECT SUM(Price) FROM Sells WHERE YEAR(SoldOn) = @year AND MONTH(SoldOn) = @MonthId)
		IF @MonthSum IS NULL
			SET @MonthSum = 0
		INSERT INTO #report VALUES (@MonthId, NULL, NULL, NULL, @MonthSum)
		SET @MonthId = @MonthId + 1
	END
	INSERT INTO #report VALUES (NULL, NULL, NULL, NULL, (SELECT SUM(Price) FROM Sells WHERE YEAR(SoldOn) = @year))
	SELECT * FROM #report
GO

EXEC ReportGeneral 2018
GO




DROP PROC ReportCars
GO

CREATE PROC ReportCars @begin DATE, @end DATE
AS
	CREATE TABLE #report (
		Brand VARCHAR(MAX),
		Model VARCHAR(MAX),
		Client VARCHAR(MAX),
		SoldOn DATE,
		Price MONEY
	)
	DECLARE @brandId UNIQUEIDENTIFIER, @brand VARCHAR(MAX),
		@modelId UNIQUEIDENTIFIER, @model VARCHAR(MAX)
	DECLARE brandCursor CURSOR FOR 
		SELECT Id, Title FROM Brands ORDER BY Title
	OPEN brandCursor
	FETCH brandCursor INTO @brandId, @brand
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE modelCursor CURSOR FOR
			SELECT Id, Title FROM Models WHERE BrandId = @brandId ORDER BY Title
		OPEN modelCursor
		FETCH modelCursor INTO @modelId, @model
		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO #report 
				SELECT @brand, @model, Clients.LastName + ' ' + Clients.FirstName, Sells.SoldOn, Sells.Price
				FROM Sells
					INNER JOIN Cars ON CarId = Cars.Id
					INNER JOIN Clients ON ClientId = Clients.Id
				WHERE ModelId = @modelId 
			IF EXISTS(SELECT *
					FROM Sells
						INNER JOIN Cars ON CarId = Cars.Id
					WHERE ModelId = @modelId
				)
			BEGIN
				DECLARE @modelSum MONEY = (SELECT SUM(Price) FROM Sells WHERE CarId IN (
						SELECT Id 
						FROM Cars 
						WHERE ModelId = @modelId
					))
				INSERT INTO #report VALUES (@brand, @model, NULL, NULL, @modelSum)
			END
			FETCH modelCursor INTO @modelId, @model
		END
		CLOSE modelCursor
		DEALLOCATE modelCursor
		IF EXISTS(SELECT * 
				FROM Sells 
					INNER JOIN Cars ON CarId = Cars.Id
					INNER JOIN Models ON ModelId = Models.Id
				WHERE BrandId = @brandId
			)
		BEGIN
			DECLARE @brandSum MONEY = (SELECT SUM(Price) FROM Sells WHERE CarId IN (
					SELECT Cars.Id 
					FROM Cars 
						INNER JOIN Models ON ModelId = Models.Id
					WHERE BrandId = @brandId
				))
			INSERT INTO #report VALUES (@brand, NULL, NULL, NULL, @brandSum)
		END
		FETCH brandCursor INTO @brandId, @brand
	END
	CLOSE brandCursor
	DEALLOCATE brandCursor
	INSERT INTO #report VALUES (NULL, NULL, NULL, NULL, (SELECT SUM(Price) FROM Sells))
	SELECT * FROM #report
GO

EXEC ReportCars '2001-01-01', '2020-01-01'

GO

DROP PROC ReportSellCars
GO

CREATE PROC ReportSellCars
AS
	CREATE TABLE #report (
		Brand VARCHAR(MAX),
		Model VARCHAR(MAX),
		ManufacturedOn DATETIME,
		Features VARCHAR(MAX),
		Price MONEY
	)
	DECLARE @brandId UNIQUEIDENTIFIER, @brand VARCHAR(MAX),
		@modelId UNIQUEIDENTIFIER, @model VARCHAR(MAX)
	DECLARE brandCursor CURSOR FOR 
		SELECT Id, Title FROM Brands ORDER BY Title
	OPEN brandCursor
	FETCH brandCursor INTO @brandId, @brand
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE modelCursor CURSOR FOR
			SELECT Id, Title FROM Models WHERE BrandId = @brandId ORDER BY Title
		OPEN modelCursor
		FETCH modelCursor INTO @modelId, @model
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE carCursor CURSOR FOR
				SELECT Id, Price, ManufacturedOn FROM Cars WHERE ModelId = @modelId AND NOT EXISTS(SELECT * FROM Sells WHERE CarId = Cars.Id)
			DECLARE @carId UNIQUEIDENTIFIER, @price MONEY, @manufacturedOn DATE
			OPEN carCursor
			FETCH carCursor INTO @carId, @price, @manufacturedOn
			WHILE @@FETCH_STATUS = 0
			BEGIN
				DECLARE @features VARCHAR(MAX) = '', @feature VARCHAR(MAX)
				DECLARE featureCursor CURSOR FOR 
					SELECT Features.Title 
					FROM CarFeatures 
						INNER JOIN Features ON FeatureId = Features.Id 
					WHERE CarId = @carId
					ORDER BY Features.Title
				OPEN featureCursor
				FETCH featureCursor INTO @feature
				WHILE @@FETCH_STATUS = 0
				BEGIN
					SET @features = @features + @feature + '; ' 
					FETCH featureCursor INTO @feature
				END
				CLOSE featureCursor
				DEALLOCATE featureCursor
				INSERT INTO #report VALUES (@brand, @model, @manufacturedOn, @features, @price)
				FETCH carCursor INTO @carId, @price, @manufacturedOn
			END
			CLOSE carCursor
			DEALLOCATE carCursor
			IF EXISTS(SELECT * FROM Cars WHERE ModelId = @modelId AND NOT EXISTS(SELECT * FROM Sells WHERE CarId = Cars.Id))
			BEGIN				
				DECLARE @modelSum MONEY = (
					SELECT SUM(Price) FROM Cars WHERE NOT EXISTS(SELECT * FROM Sells WHERE CarId = Cars.Id) AND ModelId = @modelId)
				IF @modelSum IS NULL
					SET @modelSum = 0
				INSERT INTO #report VALUES (@brand, @model, NULL, NULL, @modelSum)
			END
			FETCH modelCursor INTO @modelId, @model
		END
		CLOSE modelCursor
		DEALLOCATE modelCursor		
		IF EXISTS(SELECT * FROM Cars INNER JOIN Models ON ModelId = Models.Id WHERE BrandId = @brandId AND NOT EXISTS(SELECT * FROM Sells WHERE CarId = Cars.Id))
		BEGIN
			DECLARE @brandSum MONEY = (
				SELECT SUM(Price) FROM Cars WHERE NOT EXISTS(SELECT * FROM Sells WHERE CarId = Cars.Id)
				AND ModelId IN (SELECT Id FROM Models WHERE BrandId = @brandId)
			)
			IF @brandSum IS NULL
				SET @brandSum = 0
			INSERT INTO #report VALUES (@brand, NULL, NULL, NULL, @brandSum)
		END
		FETCH brandCursor INTO @brandId, @brand
	END
	CLOSE brandCursor
	DEALLOCATE brandCursor
	INSERT INTO #report VALUES (NULL, NULL, NULL, NULL, (SELECT SUM(Price) FROM Cars WHERE NOT EXISTS (SELECT * FROM Sells WHERE CarId = Cars.Id)))
	SELECT * FROM #report
GO

EXEC ReportSellCars

-- Brands, Models, Features, Employees?? Posts??